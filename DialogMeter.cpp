// DialogMeter.cpp : implementation file
//

#include "stdafx.h"
#include "DialogMeter.h"
#include "DataBase.h"
#include "mydefine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogMeter dialog

extern CDataBase g_dbMain;


CDialogMeter::CDialogMeter(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogMeter::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogMeter)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDialogMeter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogMeter)
	DDX_Control(pDX, IDC_EDIT_TYPE, m_edType);
	DDX_Control(pDX, IDC_LIST_METER, m_lstMeter);
	DDX_Control(pDX, IDC_EDIT_TIME_SEND, m_edTimeSend);
	DDX_Control(pDX, IDC_EDIT_TIME_RECV, m_edTimeRecv);
	DDX_Control(pDX, IDC_EDIT_DATA, m_edData);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogMeter, CDialog)
	//{{AFX_MSG_MAP(CDialogMeter)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_TYPE, OnButtonType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogMeter message handlers

BOOL CDialogMeter::OnInitDialog() 
{
	CDialog::OnInitDialog();

	DWORD dwNewStyle = LVS_EX_FULLROWSELECT;//|LVS_EX_CHECKBOXES;
	
	UINT nFlags = LVS_REPORT|LVS_SINGLESEL|LVS_SHOWSELALWAYS|LVS_OWNERDATA|LVS_NOSORTHEADER|WS_BORDER|WS_TABSTOP;
	m_lstMeter.ModifyStyle(NULL,nFlags);
//	m_lstMeter.SetDefaultStyle();
	m_lstMeter.SetExtendedStyle(dwNewStyle);	
	
	m_lstMeter.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	OnButtonRefresh();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogMeter::OnButtonRefresh() 
{
	// TODO: Add your control notification handler code here
	_METER_* pMeter = (_METER_* )m_pMeter;
	if(pMeter)
	{
		m_lstMeter.DeleteAllItems();
		
		int newHeadNum=m_lstMeter.GetHeaderCtrl()->GetItemCount();
		for(int i = 0;i < newHeadNum;i ++)
		{
			m_lstMeter.DeleteColumn(0); 
		}

		char strHEX0[1024] = {0};
		char strHEX1[1024] = {0};
		char strHEX3[1024] = {0};
		int sp = 4;
		for(int i = 0;i < 32;i ++)
		{
			char str[10] = {0};
			sprintf(str,"%02X ",pMeter->recv_data[i]);
			strcat(strHEX1,str);
			
			sprintf(str,"%02d ",i);
			strcat(strHEX0,str);
			
			if(sp == i)
			{
				strcat(strHEX0," ");
				strcat(strHEX1,",");
				sp += 3;
			}
		}
		if(pMeter->m_nType == 1)
		{
			strcat(strHEX3,"[5] [6] 已用电 ");
			strcat(strHEX3,"[8] [9] 剩余电 ");
			strcat(strHEX3,"[11] [12] 已用钱");
			strcat(strHEX3,"[14] [15] 剩余钱");
			strcat(strHEX3,"[17] [18] 电价");
			strcat(strHEX3,"\r\n");
		}
		else if(pMeter->m_nType == 2)
		{
			strcat(strHEX3,"[5-7] 已用电 ");
			strcat(strHEX3,"[8-10] 剩余电 ");
			strcat(strHEX3,"[11-13] 已用钱");
			strcat(strHEX3,"[14-16] 剩余钱");
			strcat(strHEX3,"[17-19] 电价");
			strcat(strHEX3,"[20-22] 累积");
			strcat(strHEX3,"[23-25] 瞬时");
			strcat(strHEX3,"[26-28] 户号");
			strcat(strHEX3,"\r\n");
		}
		else if(pMeter->m_nType == 3)
		{
			strcat(strHEX3,"[5] [6] 流量 ");
			strcat(strHEX3,"[8] [9] 水位 ");
			strcat(strHEX3,"[11] [12] 温度");
			strcat(strHEX3,"[14] [15] 湿度");
			strcat(strHEX3,"[17] [18] 其他");
			strcat(strHEX3,"\r\n");
		}

		{

//=======================协议V3(RTU)
/*
0x68,表号，数据长度（总包长度），控制码03，通道数量（1-255），各通道的数据，各通道的数据（从第1个开始，每通道三字节，前两个是数据，高位在前，最后一个是报警状态，1报警，0不报警），  CSL，      CSH，16H

读回的数据是：
68 07 20 03   08     00 0D   00       08 8B     00    00 06     00    04 46   00    00 32     00 00 00 00 00 00 00 00 00 00   46 14    16
                     通道1流量         水位             温度            湿度         其他                                     CRC效验
                     0X000D =0.13    0X088B=21.87    0X0006=0.06     0X0446=1094    0X0032=0.50
                    立方米            
*/
		}
		strcat(strHEX3,strHEX0);
		strcat(strHEX3,"\r\n");
		strcat(strHEX3,strHEX1);
		m_edData.SetWindowText(strHEX3);


		m_edTimeSend.SetWindowText(g_dbMain.GetTimeString(pMeter->dwSendMeterDataTime,TRUE));
		m_edTimeRecv.SetWindowText(g_dbMain.GetTimeString(pMeter->dwRecvMeterDataTime,TRUE));
		
		CString strType;
		strType.Format("%d",pMeter->m_nType);
		m_edType.SetWindowText(strType);
		
		char str[100] = {0};

		sprintf(str,"表类型 %d",pMeter->m_nType);
		SetWindowText(str);

		if(pMeter->m_nType == 1)
		{
			m_lstMeter.InsertColumn(0,"ID",LVCFMT_LEFT,40);
			m_lstMeter.InsertColumn(1,"已用电量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(2,"剩余电量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(3,"已用钱",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(4,"剩余钱",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(5,"单价",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(6,"流量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(7,"瞬间流量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(8,"当前用户",LVCFMT_RIGHT,80);
			
			// TODO: Add extra initialization here
			m_lstMeter.InsertItem(0,"");

			sprintf(str,"%d",pMeter->data1.nID);
			m_lstMeter.SetItemText(0,0,str);
			sprintf(str,"%d.%02d",pMeter->data1.nUedE / 100,pMeter->data1.nUedE % 100);
			m_lstMeter.SetItemText(0,1,str);
			sprintf(str,"%d.%02d",pMeter->data1.nLeftE / 100,pMeter->data1.nLeftE % 100);
			m_lstMeter.SetItemText(0,2,str);
			sprintf(str,"%d.%02d",pMeter->data1.nUsedM / 100,pMeter->data1.nUsedM % 100);
			m_lstMeter.SetItemText(0,3,str);
			sprintf(str,"%d",pMeter->data1.nLeftM);
			m_lstMeter.SetItemText(0,4,str);
			sprintf(str,"%d.%02d",pMeter->data1.nRate / 100,pMeter->data1.nRate % 100);
			m_lstMeter.SetItemText(0,5,str);
			sprintf(str,"%d.%02d",pMeter->data1.nFlow / 100,pMeter->data1.nFlow % 100);
			m_lstMeter.SetItemText(0,6,str);
			sprintf(str,"%d.%02d",pMeter->data1.nFlow2 / 100,pMeter->data1.nFlow2 % 100);
			m_lstMeter.SetItemText(0,7,str);
			sprintf(str,"%d",pMeter->data1.nUID);
			m_lstMeter.SetItemText(0,8,str);
		}
		else if(pMeter->m_nType == 2)
		{
			m_lstMeter.InsertColumn(0,"ID",LVCFMT_LEFT,40);
			m_lstMeter.InsertColumn(1,"已用电量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(2,"剩余电量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(3,"已用钱",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(4,"剩余钱",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(5,"单价",LVCFMT_RIGHT,50);
			m_lstMeter.InsertColumn(6,"累积流量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(7,"瞬间流量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(8,"当前用户",LVCFMT_RIGHT,80);
			
			// TODO: Add extra initialization here
			m_lstMeter.InsertItem(0,"");//已用电量         剩余电量         已用钱数       剩余钱数       电价        累积流量    瞬时流量     户号
			
			sprintf(str,"%d",pMeter->data2.nID2);
			m_lstMeter.SetItemText(0,0,str);
			sprintf(str,"%d.%02d",pMeter->data2.nUedE2 / 100,pMeter->data2.nUedE2 % 100);
			m_lstMeter.SetItemText(0,1,str);
			sprintf(str,"%d.%02d",pMeter->data2.nLeftE2 / 100,pMeter->data2.nLeftE2 % 100);
			m_lstMeter.SetItemText(0,2,str);
			sprintf(str,"%d.%02d",pMeter->data2.nUsedM2 / 100,pMeter->data2.nUsedM2 % 100);
			m_lstMeter.SetItemText(0,3,str);
			sprintf(str,"%d.%02d",pMeter->data2.nLeftM2 / 100,pMeter->data2.nLeftM2 % 100);
			m_lstMeter.SetItemText(0,4,str);
			sprintf(str,"%d.%02d",pMeter->data2.nRate2 / 100,pMeter->data2.nRate2 % 100);
			m_lstMeter.SetItemText(0,5,str);
			sprintf(str,"%d",pMeter->data2.nFlowTotal2);
			m_lstMeter.SetItemText(0,6,str);
			sprintf(str,"%d.%02d",pMeter->data2.nFlow22 / 100,pMeter->data2.nFlow22 % 100);
			m_lstMeter.SetItemText(0,7,str);
			sprintf(str,"%d",pMeter->data2.nUID2);
			m_lstMeter.SetItemText(0,8,str);
		}
		else if(pMeter->m_nType == 3)
		{
			m_lstMeter.InsertColumn(0,"ID",LVCFMT_LEFT,40);
			m_lstMeter.InsertColumn(1,"流量",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(2,"水位",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(3,"温度",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(4,"湿度",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(5,"其它1",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(6,"其它2",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(7,"其它3",LVCFMT_RIGHT,80);
			m_lstMeter.InsertColumn(8,"其它4",LVCFMT_RIGHT,80);
			
			// TODO: Add extra initialization here
			m_lstMeter.InsertItem(0,"");

			sprintf(str,"%d",pMeter->data3.nID3);
			m_lstMeter.SetItemText(0,0,str);
			sprintf(str,"%d.%02d",pMeter->data3.nFlow / 100,pMeter->data3.nFlow % 100);
			m_lstMeter.SetItemText(0,1,str);
			sprintf(str,"%d.%02d",pMeter->data3.nWater / 100,pMeter->data3.nWater % 100);
			m_lstMeter.SetItemText(0,2,str);
			sprintf(str,"%d.%02d",pMeter->data3.nTempr / 100,pMeter->data3.nTempr % 100);
			m_lstMeter.SetItemText(0,3,str);
			sprintf(str,"%d.%02d",pMeter->data3.nHumidity / 100,pMeter->data3.nHumidity % 100);
			m_lstMeter.SetItemText(0,4,str);
			sprintf(str,"%d.%02d",pMeter->data3.nOther1 / 100,pMeter->data3.nOther1 % 100);
			m_lstMeter.SetItemText(0,5,str);
			sprintf(str,"%d.%02d",pMeter->data3.nOther2 / 100,pMeter->data3.nOther2 % 100);
			m_lstMeter.SetItemText(0,6,str);
			sprintf(str,"%d.%02d",pMeter->data3.nOther3 / 100,pMeter->data3.nOther3 % 100);
			m_lstMeter.SetItemText(0,7,str);
			sprintf(str,"%d.%02d",pMeter->data3.nOther4 / 100,pMeter->data3.nOther4 % 100);
			m_lstMeter.SetItemText(0,8,str);
		}

	}
}

void CDialogMeter::OnButtonType() 
{
	// TODO: Add your control notification handler code here
	CString strType;
	m_edType.GetWindowText(strType);

	int nType = atoi(strType);
	if(nType >= 1 && nType <= 3)
	{
		_METER_* pMeter = (_METER_* )m_pMeter;
		if(pMeter)
		{
			pMeter->m_nType = nType;
			if(nType == 1)
			{
				pMeter->data1.nType1 = nType;
			}
			else if(nType == 2)
			{
				pMeter->data2.nType2 = nType;
			}
			else if(nType == 3)
			{
				pMeter->data3.nType3 = nType;
			}

			char strSQL[1024] = {0};
			sprintf(strSQL,"update WaterWellDevice set Type = %d where WaterWellID = \'%d\' and DTU_ID = \'%s\'",
				nType,pMeter->nID,pMeter->pDTU->str_ID);
			g_dbMain.ExcuteSQL(strSQL);
			g_dbMain.SendToAllClient(pMeter->recv_data,32,pMeter->pDTU,FALSE);

			OnButtonRefresh();
		}
	}
}
