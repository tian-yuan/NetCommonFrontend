#include "StdAfx.h"
#include "Locker.h"

#include "DynamicBulkSet.h"

#include "DialogView.h"

#include "DES.h"

#include "DialogConfig.h"
#include "DataBase.h"

#include "Util\log.h"
#include "Util\easylogging++.h"

#include <winsvc.h>
       
SERVICE_STATUS g_ServiceStatus; 
SERVICE_STATUS_HANDLE   g_hStatus; 
HANDLE g_hOut = 0;

pthread_mutex_t g_ctConnectLock;

pthread_mutex_t g_ctViewLock;

CWinApp g_theApp;

CDataBase g_dbMain;

  typedef union
{
    unsigned long lword;
    unsigned int word[2];
    unsigned char  byte[4];    
}Word32;

/**
 * @brief  将long型数据ex转换为BCD码
 * @param  value 传入的数据
 * @return 转换完成的BCD码
 */
unsigned long long_to_4bcd(unsigned long value)
{ 
     Word32 temp_bcd;
      unsigned char i,temp_data;
      for(i=0;i<4;i++)
        {
            temp_data=value%10;
             value=value/10;
             temp_data=(value%10)*16+temp_data;
             temp_bcd.byte[3-i]=temp_data;
             value=value/10;
        }
  return temp_bcd.lword;
}

int ascii_2_hex(unsigned char  *O_data,  unsigned char  *N_data, int  len)
   {
        int  i,j,tmp_len;
        unsigned char  tmpData;
        unsigned char  *O_buf=O_data;
        unsigned char  *N_buf=N_data;
   for(i=0;i<len;i++)
      {
          if((O_buf[i]>='0')&&(O_buf[i]<='9'))
              {
                   tmpData=O_buf[i]-'0';
              }
          else if((O_buf[i]>='A')&&(O_buf[i]<='F'))        //A......F
             {
                   tmpData=O_buf[i]-0x37;
               }
          else if((O_buf[i]>='a')&&(O_buf[i]<='f'))        //a......f
             {
                   tmpData=O_buf[i]-0x57;
               }
         else
             {

               return  -1;

              }
           O_buf[i]=tmpData;
       }
  for(tmp_len=0,j=0; j<i;j+=2)
	{
		N_buf[tmp_len++]=(O_buf[j]<<4)|O_buf[j+1];
	}

       return  tmp_len;
 }
    
int  myhex_2_ascii(unsigned char *data,  char  *buffer, int len)
{
    char   ascTable[17]={"0123456789ABCDEF"};
    char   *tmp_p=buffer;
      
    int i,pos;
    pos=0;
    for(i=0;i<len;i++) 
        {
    tmp_p[pos++]=ascTable[data[i]>>4];
    tmp_p[pos++]=ascTable[data[i]&0x0f];

        } 

    tmp_p[pos]='\0';
     
    return  pos;
   
}

BOOL Init(int argc, char** argv);

void ServiceMain(int argc, char** argv);

void  ControlHandler(DWORD request); 

void Config()
{
	g_dbMain.Config();
}

void main(int argc, char* argv[])
{
	el::Loggers::configureFromGlobal("log.conf");
//	if(argc == 2)
	{
		//if(stricmp(argv[1],"debug") == 0)
		{
			ServiceMain(2,argv);
			if (AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
			{
				CDialogView dlg;
				dlg.DoModal();
				CLocker lock(g_ctConnectLock);
				
				g_dbMain.Release();
			}
		}
	}

    SERVICE_TABLE_ENTRY ServiceTable[2];
    ServiceTable[0].lpServiceName = "_MainServer";
    ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;

    ServiceTable[1].lpServiceName = NULL;
    ServiceTable[1].lpServiceProc = NULL;
    // Start the control dispatcher thread for our service
    StartServiceCtrlDispatcher(ServiceTable);
}

extern int g_nVer;

BOOL __InitSystem(int argc, char** argv)
{
	if(argc == 2)
	{
		g_hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	}
	OutputDebug("版本 %d",g_nVer);
	OutputDebug("编译版本时间 %s",GetBuildDateTime().Format("%Y-%m-%d %H:%M:%S"));


//	int nSize = sizeof(CONNECT_INFO);

	if(!g_dbMain.LoadInitFile())
	{
		return FALSE;
	}

	OutputDebug("load init file finished.");
	g_ctConnectLock = CreateMutex(NULL, false, NULL);
	g_ctViewLock = CreateMutex(NULL, false, NULL);
	
	g_dbMain.AddODBC();
	if(g_dbMain.ConnectDB())
	{
		OutputDebug("connect db ok.");
		
	}

	g_ServiceStatus.dwServiceType = 
		SERVICE_WIN32; 
	g_ServiceStatus.dwCurrentState = 
		SERVICE_START_PENDING; 
	g_ServiceStatus.dwControlsAccepted   =  
		SERVICE_ACCEPT_STOP | 
		SERVICE_ACCEPT_SHUTDOWN;
	g_ServiceStatus.dwWin32ExitCode = 0; 
	g_ServiceStatus.dwServiceSpecificExitCode = 0; 
	g_ServiceStatus.dwCheckPoint = 0; 
	g_ServiceStatus.dwWaitHint = 0; 
	
    if(argc == 1)
	{
		g_hStatus = RegisterServiceCtrlHandler(
			"_MainServer", 
			(LPHANDLER_FUNCTION)ControlHandler); 
		if (g_hStatus == (SERVICE_STATUS_HANDLE)0) 
		{ 
			// Registering Control Handler failed
			int d = GetLastError();
			OutputDebug("RegisterServiceCtrlHandler err.%d",GetLastError());
			return FALSE; 
		}  
	}
	
	return TRUE;
}

void ControlHandler(DWORD request) 
{ 
	switch(request) 
	{ 
	case SERVICE_CONTROL_STOP: 
		OutputDebug("_MainServer stopped.");
		
		g_ServiceStatus.dwWin32ExitCode = 0; 
		g_ServiceStatus.dwCurrentState = SERVICE_STOPPED; 
		SetServiceStatus (g_hStatus, &g_ServiceStatus);
		return; 
		
	case SERVICE_CONTROL_SHUTDOWN: 
		OutputDebug("_MainServer stopped.");
		
		g_ServiceStatus.dwWin32ExitCode = 0; 
		g_ServiceStatus.dwCurrentState = SERVICE_STOPPED; 
		SetServiceStatus (g_hStatus, &g_ServiceStatus);
		return; 
        
	default:
		break;
    } 
	
    // Report current status
    SetServiceStatus (g_hStatus, &g_ServiceStatus);
	
    return; 
}

UINT ______MainThread(LPVOID pParam)//主线程
{
	SetServiceStatus (g_hStatus, &g_ServiceStatus);

	g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
	DWORD dwStartTime = GetTickCount();
	while (g_ServiceStatus.dwCurrentState == SERVICE_RUNNING)
	{
		//__A();//从DTU接收到数据先认证，然后再接收数据，接着转发到上位机PC，从PC来的数据
		       //转发到DTU数据，里面也含有心跳包的处理，

		//__B();//当上位机运行时把数据库压缩后传给上位机，对配置的数据进行接收并且刷新，还有与上位机进行的心挑包的维护


   //这里用的TCP连接说明：先打开端口，不断用非堵塞式ACCEPT（）查询是否有新连接，如果有就读出
	 //相应的序列号保存，并且在窗口上显示出来，同时去掉同样DTU号的其他连接对应的信息，下次与该DTU联系时直接使用那个序列号就可以。如果超时那么把显示在窗口
	//上的相应连接删除，等该DTU再次连接上来时，生成新的序列号再从新开始。当发送数据时返回的如果是断掉的信号（这个返回数据时XP自带的），那么
	//就去掉窗口上对应的显示，其他不变，继续等待新的连接序列号

		if((GetTickCount() - dwStartTime)/1000 > 60)
		{
			g_dbMain.LoadBaseTables2();

			dwStartTime = GetTickCount();
		}
		Sleep(100);
	}
	
	WSACleanup();

	return 0;
}

void ServiceMain(int argc, char** argv) 
{
	if(!__InitSystem(argc,argv))
	{
		return ;
	}
	OutputDebug("..........argc = %d",argc);

	{
		AfxBeginThread(______MainThread,NULL);//调试模式
	}
}



