#if !defined(AFX_DIALOGTEXT_H__39893602_BC59_4DA4_B589_8A9627D39BDB__INCLUDED_)
#define AFX_DIALOGTEXT_H__39893602_BC59_4DA4_B589_8A9627D39BDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogText.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogText dialog

#include "resource.h"

class CDialogText : public CDialog
{
// Construction
public:
	CDialogText(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogText)
	enum { IDD = IDD_DIALOG_TEXT };
	CString	m_strText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogText)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogText)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGTEXT_H__39893602_BC59_4DA4_B589_8A9627D39BDB__INCLUDED_)
