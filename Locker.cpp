// Locker.cpp: implementation of the CLocker class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Locker.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern HANDLE g_hOut;

CLocker::CLocker(pthread_mutex_t& lock,char* pName,int nLine):
m_Mutex(lock),
m_iLocked()
{
	sprintf(m_strName,"Lock>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> %s %d\n",pName,nLine);

	m_nLine = nLine;
#ifndef WIN32
	//printf(m_strName);
	m_iLocked = pthread_mutex_lock(&m_Mutex);
#else
	//OutputDebugString(m_strName);
	m_iLocked = WaitForSingleObject(m_Mutex, INFINITE);
#endif
}

// Automatically unlock in destructor
CLocker::~CLocker()
{
#ifndef WIN32
	//printf(m_strName);
	if (0 == m_iLocked)
		pthread_mutex_unlock(&m_Mutex);
#else
	//OutputDebugString(m_strName);
	if (WAIT_FAILED != m_iLocked)
		ReleaseMutex(m_Mutex);
#endif/**/
}

void CLocker::enterCS(pthread_mutex_t& lock)
{
#ifndef WIN32
	pthread_mutex_lock(&lock);
#else
	WaitForSingleObject(lock, INFINITE);
#endif
}

void CLocker::leaveCS(pthread_mutex_t& lock)
{
#ifndef WIN32
	pthread_mutex_unlock(&lock);
#else
	ReleaseMutex(lock);
#endif
}

char* Byte2String(BYTE* pBuff,int nLen)
{
	static char buff[1024] = {0};
	memset(buff,0,sizeof(buff));

	if(nLen <= 400)
	{
		for(int i = 0;i < nLen;i ++)
		{
			char bbb[20] = {0};
			sprintf(bbb,"%02X ",pBuff[i]);
			strcat(buff,bbb);
		}
	}
	return buff;
}
/*
void OutputDebug(const char * fmt, ...)
{
    char szData[512] = {0};
    char data[512] = {0};
    memset(data,0,sizeof(data));
	
    va_list args;
	va_start(args, fmt);
	_vsnprintf(szData, sizeof(szData) - 1, fmt, args);
	va_end(args);
	
	SYSTEMTIME sys;
	GetLocalTime(&sys);
	
	sprintf(data,"%s %s\n",CTime::GetCurrentTime().Format("%H:%M:%S"),szData);
	//sys.wMinute,
	OutputDebugString(data);
	
	if(g_hOut != 0)
	{
		_tprintf(_T("%s"), data);
	}
}
*/

