// DialogConfig.cpp : implementation file
//

#include "stdafx.h"
#include "DialogConfig.h"
#include "DataBase.h"
#include "mydefine.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CDialogConfig dialog


CDialogConfig::CDialogConfig(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogConfig::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogConfig)
	m_nPortA = 0;
	m_nTimeOut = 0;
	m_nPortB = 0;
	m_nCheckTime = 0;
	m_nLogDay = 0;
	m_strName = _T("");
	m_nLogLine = 0;
	//}}AFX_DATA_INIT
    

}


void CDialogConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogConfig)
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPortA);
	DDX_Text(pDX, IDC_EDIT_TIMEOUT, m_nTimeOut);
	DDX_Text(pDX, IDC_EDIT_PORT2, m_nPortB);
	DDX_Text(pDX, IDC_EDIT_TIME, m_nCheckTime);
	DDX_Text(pDX, IDC_EDIT_LOG, m_nLogDay);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Text(pDX, IDC_EDIT_LINE, m_nLogLine);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogConfig, CDialog)
	//{{AFX_MSG_MAP(CDialogConfig)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDCANCEL2, &CDialogConfig::OnBnClickedCancel2)
	ON_BN_CLICKED(IDCANCEL3, &CDialogConfig::OnBnClickedCancel3)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogConfig message handlers

void CDialogConfig::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

void CDialogConfig::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}


#ifdef connectSql2008



extern  CDataBase g_dbMain;
void CDialogConfig::OnBnClickedCancel2()
{
	// CDataBase g_dbMain;

	g_dbMain.testmengON();

	// TODO: 在此添加控件通知处理程序代码
}


void CDialogConfig::OnBnClickedCancel3()
{
	

	g_dbMain.testmengOFF();
	// TODO: 在此添加控件通知处理程序代码
}
#endif