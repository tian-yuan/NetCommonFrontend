#if !defined(AFX_DIALOGVIEW_H__2D54893F_57E0_4902_A78F_BA1E5B396EFD__INCLUDED_)
#define AFX_DIALOGVIEW_H__2D54893F_57E0_4902_A78F_BA1E5B396EFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogView.h : header file
//
#include <afxcmn.h>	

/////////////////////////////////////////////////////////////////////////////
// CDialogView dialog
#include "resource.h"
#include "VirtualListCtrl.h"

class CDialogView : public CDialog
{
// Construction
public:
	void UpdateList2();
	void UpdateList1();
	CDialogView(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogView)
	enum { IDD = IDD_DIALOG_VIEW };
	CButton	m_edConfig;
	CButton	m_edLoad;
	CButton	m_edReg;
	CButton	m_edDel;
	CButton	m_edCheck;
	CButton	m_edExit;
	CVirtualListCtrl	m_lstConnect;
	CVirtualListCtrl	m_lstClient;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogView)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTcpClose();
	afx_msg void OnRclickListConnect(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTcpSend();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonReg();
	afx_msg void OnButtonDel();
	afx_msg void OnButtonCheck();
	afx_msg void OnGetdispinfoListConnect(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonConfig();
	afx_msg void OnDblclkListConnect(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClientRemove();
	afx_msg void OnRclickListWait(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClientClear();
	afx_msg void OnClientRefresh();
	afx_msg void OnButtonLoad();
	virtual void OnCancel();
	//}}AFX_MSG
	afx_msg LRESULT  OnNcHitTest(CPoint pt);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGVIEW_H__2D54893F_57E0_4902_A78F_BA1E5B396EFD__INCLUDED_)
