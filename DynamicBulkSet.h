#if !defined(AFX_DYNAMICBULKSET_H__95E34C47_4CFB_4FE9_BADE_71C53BE7C2B2__INCLUDED_)
#define AFX_DYNAMICBULKSET_H__95E34C47_4CFB_4FE9_BADE_71C53BE7C2B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynamicBulkSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDynamicBulkSet recordset

#define MAX_TEXT_LEN 400 // This is about as much as will fit in UI

class CDynamicBulkSet : public CRecordset
{
public:
	CDynamicBulkSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CDynamicBulkSet)
		
	BOOL Open(LPCTSTR lpszSQL);
	virtual void Close();

	void** m_ppvData;
	void** m_ppvLengths;
	
	virtual void DoBulkFieldExchange(CFieldExchange* pFX);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICBULKSET_H__95E34C47_4CFB_4FE9_BADE_71C53BE7C2B2__INCLUDED_)
