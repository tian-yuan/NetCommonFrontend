// Location.h: interface for the CLocation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCATION_H__6E5DD4BA_02E6_4B60_A4BA_483FB03F3B4E__INCLUDED_)
#define AFX_LOCATION_H__6E5DD4BA_02E6_4B60_A4BA_483FB03F3B4E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct _IpLocation 
{ 
	unsigned int IpStart; 
	unsigned int IpEnd; 
	char chCountry[1024]; 
	char chCity[1024]; 
}IpLocation;



class CLocation  
{
	char m_strFileName[MAX_PATH];//IPshuju 文件

public:
	CLocation(char* pFileName);
	virtual ~CLocation();

private:
	//IP字符串 -> IP数值 
	unsigned int IpStringToInt(const char * ipStr) ;
	// 采用"二分法"搜索索引区, 定位IP索引记录位置 
	int getIndexOffset(FILE * fp, int fo, int lo, unsigned int ipv);

	// 读取IP所在地字符串 
	char * getString(char * strBuf, FILE * fp);
	//查询IP段和所在地 
	BOOL GetIpLocation(unsigned int ipv, IpLocation &Ipl) ;
	CString IpLocating(const char * ipStr);
public:
	BOOL GetLocation(const char * ipStr,char* pLocation);
};

#endif // !defined(AFX_LOCATION_H__6E5DD4BA_02E6_4B60_A4BA_483FB03F3B4E__INCLUDED_)
