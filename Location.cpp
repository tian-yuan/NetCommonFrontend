// Location.cpp: implementation of the CLocation class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Location.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define STR_BUFF_SIZE 128

CLocation::CLocation(char* pFileName)
{
	sprintf(m_strFileName,"%s",pFileName);

}

CLocation::~CLocation()
{
}

//IP字符串 -> IP数值 
unsigned int CLocation::IpStringToInt(const char * ipStr) 
{ 
	int t[4];
	int nport;
	unsigned int ipv; 
	unsigned char * p=(unsigned char *)&ipv; 
	sscanf(ipStr,"%d.%d.%d.%d:%d",&t[0],&t[1],&t[2],&t[3],&nport); 
	p[0]=t[3]; p[1]=t[2]; p[2]=t[1]; p[3]=t[0]; 

	return ipv; 
} 
// 采用"二分法"搜索索引区, 定位IP索引记录位置 
int CLocation::getIndexOffset(FILE * fp, int fo, int lo, unsigned int ipv) 
{ 
	int mo; //中间偏移量 
	unsigned int mv; //中间值 
	unsigned int fv,lv; //边界值 
	unsigned int llv; //边界末末值 
	fseek(fp,fo,SEEK_SET); 
	fread(&fv,4,1,fp); 
	fseek(fp,lo,SEEK_SET); 
	fread(&lv,4,1,fp); 
	//临时作它用,末记录体偏移量 
	fread(&mo,3,1,fp); mo&=0xffffff; 
	fseek(fp,mo,SEEK_SET); 
	fread(&llv,sizeof(int),1,fp); 
	//printf("%d %d %d %d %d %d %d n",fo,lo,mo,ipv,fv,lv,llv); 
	//边界检测处理 
	if(ipv<fv) 
		return -1; 
	else if(ipv>llv) 
		return -1; 
	//使用"二分法"确定记录偏移量 
	do 
	{ 
		mo=fo+(lo-fo)/7/2*7; 
		fseek(fp,mo,SEEK_SET); 
		fread(&mv,sizeof(int),1,fp); 
		if(ipv>=mv) 
			fo=mo; 
		else 
			lo=mo; 
		if(lo-fo==7) 
			mo=lo=fo; 
	} while(fo!=lo); 
	return mo; 
} 

// 读取IP所在地字符串 
char * CLocation::getString(char * strBuf, FILE * fp) 
{ 
	char tag; 
	int so; 
	fread(&tag,sizeof(char),1,fp); 
	if(tag==0x01)//重定向模式1: 城市信息随国家信息定向 
	{ 
		fread(&so,3,1,fp); so&=0xffffff; 
		fseek(fp,so,SEEK_SET); 
		return getString(strBuf,fp); 
	} 
	else if(tag==0x02)//重定向模式2: 城市信息没有随国家信息定向 
	{ 
		fread(&so,3,1,fp); so&=0xffffff; 
		int tmo=ftell(fp);//记下文件当前读位置 
		fseek(fp,so,SEEK_SET); 
		getString(strBuf,fp); 
		fseek(fp,tmo,SEEK_SET); 
		return strBuf; 
	} 
	else //无重定向: 最简单模式 
	{ 
		fseek(fp,-1,SEEK_CUR); 
		fread(strBuf,sizeof(char),STR_BUFF_SIZE,fp);//读取字符串 
		fseek(fp,strlen(strBuf)+1-STR_BUFF_SIZE,SEEK_CUR);//修正文件指针 
		return strBuf; 
	} 
} 
//查询IP段和所在地 
BOOL CLocation::GetIpLocation(unsigned int ipv, IpLocation &Ipl) 
{
//	WriteToLog("open file..");
	FILE * fp=fopen(m_strFileName,"rb");
	
	if(!fp) 
	{ 
		return FALSE;
	} 
	int fo,lo; //首末索引偏移量 
	fread(&fo,sizeof(int),1,fp); 
	fread(&lo,sizeof(int),1,fp); 
	//获取索引记录偏移量 
	int rcOffset = getIndexOffset(fp,fo,lo,ipv); 
//	WriteToLog("open file..%d",rcOffset);
	fseek(fp,rcOffset,SEEK_SET); 
	
	BOOL bFind = FALSE;
	if(rcOffset >= 0) 
	{ 
		fseek(fp,rcOffset,SEEK_SET); 
		//读取开头IP值 
		fread(&Ipl.IpStart,sizeof(int),1,fp); 
		//转到记录体 
		int ro; //记录体偏移量 
		fread(&ro,3,1,fp); 
		ro &= 0xffffff; 
		fseek(fp,ro,SEEK_SET); 
		//读取结尾IP值 
		fread(&Ipl.IpEnd,sizeof(int),1,fp); 
		char strBuf[STR_BUFF_SIZE]; 
		getString(strBuf,fp); 
		strcpy(Ipl.chCountry,strBuf); 
		getString(strBuf,fp); 
		strcpy(Ipl.chCity,strBuf);

		bFind = TRUE;
	} 
	else 
	{ 
		//没找到 
		//		Ipl.IpStart=0; 
		//		Ipl.IpEnd=0; 
		//		Ipl.Country="未知国家"; 
		//		Ipl.City="未知地址"; 
	} 
	fclose(fp); 
	return bFind; 
} 

CString CLocation::IpLocating(const char * ipStr)
{
	CString str;
	unsigned int ipv = IpStringToInt(ipStr); 
	IpLocation ipl; 
	memset(ipl.chCity, 0, 1024);
	memset(ipl.chCountry, 0, 1024);
	if(!GetIpLocation(ipv,ipl))
	{
		str = ipStr;
		return str;
	}
	
	str.Format("%s(%s)", ipl.chCountry, ipl.chCity);
	
	return str;
}

BOOL CLocation::GetLocation(const char * ipStr,char* pLocation)
{
//	WriteToLog("%d %s",__LINE__,ipStr);
	strcpy(pLocation,IpLocating(ipStr));
//	WriteToLog("%d %s over",__LINE__,ipStr);
	

	return FALSE;
}

