// Locker.h: interface for the CLocker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCKER_H__138A44D4_41CA_4E57_AF66_79DD57244DD6__INCLUDED_)
#define AFX_LOCKER_H__138A44D4_41CA_4E57_AF66_79DD57244DD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UserDefine.h"
#include "Util\log.h"

CTime GetBuildDateTime();

class CLocker  
{
public:
	CLocker(pthread_mutex_t& lock,char* pName = "",int nLine = 0);
	~CLocker();
	
	static void enterCS(pthread_mutex_t& lock);
	static void leaveCS(pthread_mutex_t& lock);
	
private:
	pthread_mutex_t& m_Mutex;            // Alias name of the mutex to be protected
	int m_iLocked;                       // Locking status
	
	char m_strName[MAX_PATH];
	int m_nLine;

};
/*
typedef struct CONNECT_INFO//连接信息结构
{
	SOCKET sSocket;//套接字
	char strFromIP[30];//连接地址
	int nFromPort;//连接端口
	DWORD dwConnectTime;//连接时间
	BOOL bTCP;//是否是TCP
	DWORD dwLastDTURecv;//最后接收 DUT
	DWORD dwLastMeterRecv;//最后接收 METER
	DWORD dwLastSend;//最后发送

	char strDTU[32];//DTU ID from DB
	char strMeter[32];//METER ID from DB
	char strName[64];//Name from DB·

	int nArea;//from db

}CONNECT_INFO;

typedef std::map<SOCKET, CONNECT_INFO > WAIT_LIST;//按套接字保存 等待超时后关闭 或 收到认证包

typedef std::map<std::string, CONNECT_INFO* > CONNECT_LIST;//按ID号 保存

typedef struct VIEW_INFO
{
	SOCKET sSocket;//套接字
	char strFromIP[30];//连接地址
	int nFromPort;//连接端口

	DWORD dwRecvTime;
	DWORD dwSendTime;
}VIEW_INFO;

typedef std::map<SOCKET, VIEW_INFO* > VIEW_LIST_VIEW;

typedef struct COMMAND_INFO
{
	int nTaskId;
}COMMAND_INFO;


*/
//void OutputDebug(const char * fmt, ...);



#endif // !defined(AFX_LOCKER_H__138A44D4_41CA_4E57_AF66_79DD57244DD6__INCLUDED_)
