#if !defined(AFX_VIRTUALLISTCTRL_H__B6A867F0_CC40_49BC_985F_44E5E8B86AEB__INCLUDED_)
#define AFX_VIRTUALLISTCTRL_H__B6A867F0_CC40_49BC_985F_44E5E8B86AEB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VirtualListCtrl.h : header file
//
//虚拟列表控件
/////////////////////////////////////////////////////////////////////////////
// CVirtualListCtrl window

//How to use
/*
1 SetDefaultStyle [SetExtendedStyle]
2 InsertColumn[InsertColumn,InsertColumn,InsertColumn]
3 InsertData[InsertData,InsertData,InsertData,InsertData]
4 SetCount
5 [GetItemText,GetItemText,SetItemText,SetItemText]
*/

class CItemData
{
public:
	CItemData();
	~CItemData();

	CString m_strData[30];//默认30列 可以自行修改
	void SetItemData(int nColumn,CString strData);
};

#include <vector>
using namespace std;

class CVirtualListCtrl : public CListCtrl
{
// Construction
public:
	CVirtualListCtrl();

// Attributes
public:
private:
	vector<CItemData> m_database;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVirtualListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetDefaultStyle();
	void InsertData(CItemData info);
	void SetCount();
	int GetCount();
	
	void SetItemText(int nItem, int nSubItem,CString strData);
	CString GetItemText(int nItem, int nSubItem);

	BOOL DeleteItem(int nItem);

	virtual ~CVirtualListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CVirtualListCtrl)
	afx_msg void OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOdfinditem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOdcachehint(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIRTUALLISTCTRL_H__B6A867F0_CC40_49BC_985F_44E5E8B86AEB__INCLUDED_)
