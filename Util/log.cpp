#include "log.h"
#include <stdarg.h>
#include <time.h>

#define ELPP_NO_DEFAULT_LOG_FILE
#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP;

void OutputDebug(const char * fmt, ...)
{
	char szData[512] = { 0 };

#ifdef WIN32	
	char data[512] = { 0 };
	va_list args;
	va_start(args, fmt);
	_vsnprintf_s(szData, sizeof(szData) - 1, fmt, args);
	va_end(args);

	SYSTEMTIME sys;
	GetLocalTime(&sys);

	sprintf_s(data, "%02d:%02d:%02d %s\n", sys.wHour, sys.wMinute, sys.wSecond, szData);
	LOG(INFO) << data;
#else
	va_list args;
	int n;
	va_start(args, fmt);
	n = vsprintf(szData, fmt, args);
	va_end(args);

	time_t now = time(0);
	tm *tnow = localtime(&now);

	printf("%02d:%02d:%02d %s\n", tnow->tm_hour, tnow->tm_min, tnow->tm_sec, szData);
#endif
}