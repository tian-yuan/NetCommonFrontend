#if !defined(AFX_DIALOGCONFIG_H__7D99CAE2_5E40_4D00_AF6A_50274D8D23D2__INCLUDED_)
#define AFX_DIALOGCONFIG_H__7D99CAE2_5E40_4D00_AF6A_50274D8D23D2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogConfig dialog
#include "resource.h"

class CDialogConfig : public CDialog
{
// Construction
public:
	CDialogConfig(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogConfig)
	enum { IDD = IDD_DIALOG_CONFIG };
	int		m_nPortA;
	int		m_nTimeOut;
	int		m_nPortB;
	int		m_nCheckTime;
	int		m_nLogDay;
	CString	m_strName;
	int		m_nLogLine;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogConfig)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogConfig)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel2();
	afx_msg void OnBnClickedCancel3();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGCONFIG_H__7D99CAE2_5E40_4D00_AF6A_50274D8D23D2__INCLUDED_)
