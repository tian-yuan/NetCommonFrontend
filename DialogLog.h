#if !defined(AFX_DIALOGLOG_H__85BD435F_642E_44E9_B725_69D2B068405D__INCLUDED_)
#define AFX_DIALOGLOG_H__85BD435F_642E_44E9_B725_69D2B068405D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogLog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogLog dialog
#include "resource.h"

class CDialogLog : public CDialog
{
// Construction
public:
	CDialogLog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogLog)
	enum { IDD = IDD_DIALOG_LOG };
	CListCtrl	m_lstLog;
	//}}AFX_DATA

	void* m_pDTU;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogLog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogLog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGLOG_H__85BD435F_642E_44E9_B725_69D2B068405D__INCLUDED_)
