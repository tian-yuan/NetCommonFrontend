// DialogDTU.cpp : implementation file
//

#include "stdafx.h"
#include "DialogDTU.h"
#include "DialogLog.h"
#include "DataBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogDTU dialog

extern CDataBase g_dbMain;
CDialogDTU::CDialogDTU(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDTU::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogDTU)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDialogDTU::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogDTU)
	DDX_Control(pDX, IDC_EDIT_NUM, m_edNum);
	DDX_Control(pDX, IDC_LIST_METER_ALL, m_lstMeter);
	DDX_Control(pDX, IDC_EDIT_TIME_ACTIVE, m_edActiveTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogDTU, CDialog)
	//{{AFX_MSG_MAP(CDialogDTU)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_COMM, OnButtonComm)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogDTU message handlers

BOOL CDialogDTU::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	DWORD dwNewStyle = LVS_EX_FULLROWSELECT;//|LVS_EX_CHECKBOXES;
	
	UINT nFlags = LVS_REPORT|LVS_SINGLESEL|LVS_SHOWSELALWAYS|LVS_OWNERDATA|LVS_NOSORTHEADER|WS_BORDER|WS_TABSTOP;
	m_lstMeter.ModifyStyle(NULL,nFlags);
	//	m_lstMeter.SetDefaultStyle();
	m_lstMeter.SetExtendedStyle(dwNewStyle);	
	
	m_lstMeter.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_lstMeter.InsertColumn(0," ",LVCFMT_LEFT,30);
	m_lstMeter.InsertColumn(1," ",LVCFMT_LEFT,60);
	m_lstMeter.InsertColumn(2," ",LVCFMT_RIGHT,80);
	m_lstMeter.InsertColumn(3," ",LVCFMT_RIGHT,80);
	m_lstMeter.InsertColumn(4," ",LVCFMT_RIGHT,80);
	m_lstMeter.InsertColumn(5," ",LVCFMT_RIGHT,80);
	m_lstMeter.InsertColumn(6," ",LVCFMT_RIGHT,55);
	m_lstMeter.InsertColumn(7," ",LVCFMT_RIGHT,55);
	m_lstMeter.InsertColumn(8," ",LVCFMT_RIGHT,80);
	m_lstMeter.InsertColumn(9," ",LVCFMT_RIGHT,80);
	
	// TODO: Add extra initialization here
	
	OnButtonRefresh();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogDTU::OnButtonRefresh() 
{
	// TODO: Add your control notification handler code here
	_DTU_* pDTU = (_DTU_* )m_pDTU;
	if(pDTU)
	{
		m_edActiveTime.SetWindowText(g_dbMain.GetTimeString(pDTU->dwRecvActiveTime,TRUE));

		char num[32] = {0};
		sprintf(num,"%d",pDTU->m_MeterList.GetCount());
		m_edNum.SetWindowText(num);

		m_lstMeter.DeleteAllItems();
		int j = 0;
		for(POSITION pos = pDTU->m_MeterList.GetHeadPosition();pos;j ++)
		{
			_METER_* pMeter = (_METER_* )pDTU->m_MeterList.GetNext(pos);

			m_lstMeter.InsertItem(j,"");
			char str[100] = {0};
			if(pMeter->m_nType == 1)
			{
				m_lstMeter.SetItemText(j,0,"ID");
				m_lstMeter.SetItemText(j,1,"类型");
				m_lstMeter.SetItemText(j,2,"已用电量");
				m_lstMeter.SetItemText(j,3,"剩余电量");
				m_lstMeter.SetItemText(j,4,"已用钱");
				m_lstMeter.SetItemText(j,5,"剩余钱");
				m_lstMeter.SetItemText(j,6,"单价");
				m_lstMeter.SetItemText(j,7,"流量");
				m_lstMeter.SetItemText(j,8,"瞬间流量");
				m_lstMeter.SetItemText(j,9,"当前用户");
				j ++;
				m_lstMeter.InsertItem(j,"");


				sprintf(str,"%d",pMeter->data1.nID);
				m_lstMeter.SetItemText(j,0,str);

				sprintf(str,"%d",pMeter->m_nType);
				m_lstMeter.SetItemText(j,1,str);

				sprintf(str,"%d.%02d",pMeter->data1.nUedE / 100,pMeter->data1.nUedE % 100);
				m_lstMeter.SetItemText(j,2,str);
				sprintf(str,"%d.%02d",pMeter->data1.nLeftE / 100,pMeter->data1.nLeftE % 100);
				m_lstMeter.SetItemText(j,3,str);
				sprintf(str,"%d.%02d",pMeter->data1.nUsedM / 100,pMeter->data1.nUsedM % 100);
				m_lstMeter.SetItemText(j,4,str);
				sprintf(str,"%d",pMeter->data1.nLeftM);
				m_lstMeter.SetItemText(j,5,str);
				sprintf(str,"%d.%02d",pMeter->data1.nRate / 100,pMeter->data1.nRate % 100);
				m_lstMeter.SetItemText(j,6,str);
				sprintf(str,"%d.%02d",pMeter->data1.nFlow / 100,pMeter->data1.nFlow % 100);
				m_lstMeter.SetItemText(j,7,str);
				sprintf(str,"%d.%02d",pMeter->data1.nFlow2 / 100,pMeter->data1.nFlow2 % 100);
				m_lstMeter.SetItemText(j,8,str);
				sprintf(str,"%d",pMeter->data1.nUID);
				m_lstMeter.SetItemText(j,9,str);

			}
			else if(pMeter->m_nType == 2)
			{
				m_lstMeter.SetItemText(j,0,"ID");
				m_lstMeter.SetItemText(j,1,"类型");
				m_lstMeter.SetItemText(j,2,"已用电量");
				m_lstMeter.SetItemText(j,3,"剩余电量");
				m_lstMeter.SetItemText(j,4,"已用钱");
				m_lstMeter.SetItemText(j,5,"剩余钱");
				m_lstMeter.SetItemText(j,6,"单价");
				m_lstMeter.SetItemText(j,7,"流量");
				m_lstMeter.SetItemText(j,8,"瞬间流量");
				m_lstMeter.SetItemText(j,9,"当前用户");
				j ++;
				m_lstMeter.InsertItem(j,"");


				sprintf(str,"%d",pMeter->data2.nID2);
				m_lstMeter.SetItemText(j,0,str);

				sprintf(str,"%d",pMeter->m_nType);
				m_lstMeter.SetItemText(j,1,str);

				sprintf(str,"%d.%02d",pMeter->data2.nUedE2 / 100,pMeter->data2.nUedE2 % 100);
				m_lstMeter.SetItemText(j,2,str);
				sprintf(str,"%d.%02d",pMeter->data2.nLeftE2 / 100,pMeter->data2.nLeftE2 % 100);
				m_lstMeter.SetItemText(j,3,str);
				sprintf(str,"%d.%02d",pMeter->data2.nUsedM2 / 100,pMeter->data2.nUsedM2 % 100);
				m_lstMeter.SetItemText(j,4,str);
				sprintf(str,"%d.%02d",pMeter->data2.nLeftM2 / 100,pMeter->data2.nLeftM2 % 100);
				m_lstMeter.SetItemText(j,5,str);
				sprintf(str,"%d.%02d",pMeter->data2.nRate2 / 100,pMeter->data2.nRate2 % 100);
				m_lstMeter.SetItemText(j,6,str);
				sprintf(str,"%d",pMeter->data2.nFlowTotal2);
				m_lstMeter.SetItemText(j,7,str);
				sprintf(str,"%d.%02d",pMeter->data2.nFlow22 / 100,pMeter->data2.nFlow22 % 100);
				m_lstMeter.SetItemText(j,8,str);
				sprintf(str,"%d",pMeter->data2.nUID2);
				m_lstMeter.SetItemText(j,9,str);

			}
			else if(pMeter->m_nType == 3)
			{
				m_lstMeter.SetItemText(j,0,"ID");
				m_lstMeter.SetItemText(j,1,"类型");
				m_lstMeter.SetItemText(j,2,"流量");    
				m_lstMeter.SetItemText(j,3,"水位");
				m_lstMeter.SetItemText(j,4,"温度");
				m_lstMeter.SetItemText(j,5,"湿度");
				m_lstMeter.SetItemText(j,6,"其它1");
				m_lstMeter.SetItemText(j,7,"其它2");
				m_lstMeter.SetItemText(j,8,"其它3");
				m_lstMeter.SetItemText(j,9,"其它4");
				j ++;
				m_lstMeter.InsertItem(j,"");


				sprintf(str,"%d",pMeter->data3.nID3);
				m_lstMeter.SetItemText(j,0,str);

				sprintf(str,"%d",pMeter->m_nType);
				m_lstMeter.SetItemText(j,1,str);

				sprintf(str,"%d.%02d",pMeter->data3.nFlow / 100,pMeter->data3.nFlow % 100);
				m_lstMeter.SetItemText(j,2,str);
				sprintf(str,"%d.%02d",pMeter->data3.nWater / 100,pMeter->data3.nWater % 100);
				m_lstMeter.SetItemText(j,3,str);
				sprintf(str,"%d.%02d",pMeter->data3.nTempr / 100,pMeter->data3.nTempr % 100);
				m_lstMeter.SetItemText(j,4,str);
				sprintf(str,"%d.%02d",pMeter->data3.nHumidity / 100,pMeter->data3.nHumidity % 100);
				m_lstMeter.SetItemText(j,5,str);
				sprintf(str,"%d.%02d",pMeter->data3.nOther1 / 100,pMeter->data3.nOther1 % 100);
				m_lstMeter.SetItemText(j,6,str);
				sprintf(str,"%d.%02d",pMeter->data3.nOther2 / 100,pMeter->data3.nOther2 % 100);
				m_lstMeter.SetItemText(j,7,str);
				sprintf(str,"%d.%02d",pMeter->data3.nOther3 / 100,pMeter->data3.nOther3 % 100);
				m_lstMeter.SetItemText(j,8,str);
				sprintf(str,"%d.%02d",pMeter->data3.nOther4 / 100,pMeter->data3.nOther4 % 100);
				m_lstMeter.SetItemText(j,9,str);

			}
		}

	}
}

void CDialogDTU::OnButtonComm() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnCancel();
	CDialogLog dlg;

	dlg.m_pDTU = (_DTU_* )m_pDTU;
	dlg.DoModal();
}
