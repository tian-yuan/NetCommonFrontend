// VirtualListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "VirtualListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CItemData::CItemData()
{
}

CItemData::~CItemData()
{
}


void CItemData::SetItemData(int nColumn,CString strData)
{
	m_strData[nColumn].Format("%s",strData);
}

/////////////////////////////////////////////////////////////////////////////
// CVirtualListCtrl

CVirtualListCtrl::CVirtualListCtrl()
{
}

CVirtualListCtrl::~CVirtualListCtrl()
{
	m_database.clear();
}


BEGIN_MESSAGE_MAP(CVirtualListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CVirtualListCtrl)
	ON_NOTIFY_REFLECT(LVN_GETDISPINFO, OnGetdispinfo)
	ON_NOTIFY_REFLECT(LVN_ODFINDITEM, OnOdfinditem)
	ON_NOTIFY_REFLECT(LVN_ODCACHEHINT, OnOdcachehint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVirtualListCtrl message handlers

void CVirtualListCtrl::OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	LV_ITEM* pItem= &(pDispInfo)->item;
	
	int itemid = pItem->iItem;
	
	if (pItem->mask & LVIF_TEXT)
	{
		lstrcpyn(pItem->pszText, m_database[itemid].m_strData[pItem->iSubItem], pItem->cchTextMax);
	}
	
	*pResult = 0;
}

void CVirtualListCtrl::OnOdfinditem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLVFINDITEM* pFindInfo = (NMLVFINDITEM*)pNMHDR;
	
	*pResult = -1;
	
	if( (pFindInfo->lvfi.flags & LVFI_STRING) == 0 )
	{
		return;
	}
	
	
	CString searchstr = pFindInfo->lvfi.psz;
	
	int startPos = pFindInfo->iStart;
	if(startPos >= GetItemCount())
		startPos = 0;
	
	int currentPos=startPos;
	
	do
	{		
		if( _tcsnicmp(m_database[currentPos].m_strData[0], searchstr, searchstr.GetLength()) == 0)
		{
			*pResult = currentPos;
			break;
		}
		
		currentPos++;
		
		if(currentPos >= GetItemCount())
			currentPos = 0;
		
	}while(currentPos != startPos);	
}

void CVirtualListCtrl::OnOdcachehint(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLVCACHEHINT* pCacheHint = (NMLVCACHEHINT*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CVirtualListCtrl::InsertData(CItemData info)
{
	m_database.push_back(info);
}

void CVirtualListCtrl::SetCount()
{
	int n = GetItemCount();
	if(n != m_database.size())
		SetItemCount(m_database.size());
}

int CVirtualListCtrl::GetCount()
{
	return m_database.size();
}

CString CVirtualListCtrl::GetItemText(int nItem, int nSubItem)
{
	if(nItem >= m_database.size())
		return "";
	else
		return m_database[nItem].m_strData[nSubItem];
}

void CVirtualListCtrl::SetItemText(int nItem, int nSubItem,CString strData)
{
	if(nItem >= m_database.size())
	{
		return ;
	}
	else
	{
		if(m_database[nItem].m_strData[nSubItem] != strData)
		{
			m_database[nItem].m_strData[nSubItem] = strData;

			//暂时这样处理
			int nSel = -1;
			POSITION pos = GetFirstSelectedItemPosition();
			if(pos)
			{
				nSel = GetNextSelectedItem(pos);
			}
			if(GetItemState(nItem, LVIS_SELECTED) == LVIS_SELECTED)
			{
				SetItemState(nItem, 0, LVIS_SELECTED|LVIS_FOCUSED);
			}
			else
			{
				SetItemState(nItem, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
			}
			if(nSel != -1)
			{
				SetItemState(nSel, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
			}
		}
	}
}

void CVirtualListCtrl::SetDefaultStyle()
{
	UINT nFlags = LVS_REPORT|LVS_SINGLESEL|LVS_SHOWSELALWAYS|LVS_OWNERDATA|LVS_NOSORTHEADER|WS_BORDER|WS_TABSTOP;
	ModifyStyle(NULL,nFlags);
}

BOOL CVirtualListCtrl::DeleteItem(int nItem)
{
	m_database.erase(m_database.begin() + nItem);
	SetCount();

	return TRUE;
}
