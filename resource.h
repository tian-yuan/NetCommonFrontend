//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 MainServer.rc 使用
//
#define IDCANCEL2                       3
#define IDCANCEL3                       4
#define IDD_DIALOG_VIEW                 101
#define IDR_MENU_POP                    102
#define IDD_DIALOG_TEXT                 103
#define IDI_MAIN                        104
#define IDD_DIALOG_CONFIG               105
#define IDD_DIALOG_METER                106
#define IDD_DIALOG_DTU                  107
#define IDD_DIALOG_LOG                  108
#define IDC_LIST_CONNECT                1000
#define IDC_EDIT_TEXT                   1001
#define IDC_LIST_WAIT                   1001
#define IDC_BUTTON_REG                  1002
#define IDC_BUTTON_DEL                  1003
#define IDC_EDIT_PORT                   1003
#define IDC_BUTTON_CHECK                1004
#define IDC_EDIT_TIMEOUT                1004
#define IDC_BUTTON_CONFIG               1005
#define IDC_EDIT_DSN                    1005
#define IDC_EDIT_LOG                    1005
#define IDC_EDIT_USER                   1006
#define IDC_EDIT_DATA                   1006
#define IDC_BUTTON_LOAD                 1006
#define IDC_EDIT_LINE                   1006
#define IDC_EDIT_PWD                    1007
#define IDC_EDIT_TIME_SEND              1007
#define IDC_EDIT_PORT2                  1008
#define IDC_EDIT_TIME_RECV              1008
#define IDC_EDIT_TIME                   1009
#define IDC_LIST_METER                  1009
#define IDC_BUTTON_REFRESH              1010
#define IDC_EDIT_TIME_ACTIVE            1011
#define IDC_LIST_METER_ALL              1012
#define IDC_EDIT_NUM                    1013
#define IDC_EDIT_TYPE                   1013
#define IDC_BUTTON_TYPE                 1014
#define IDC_EDIT_NAME                   1015
#define IDC_BUTTON_COMM                 1016
#define IDC_LIST_RECV                   1017
#define ID_TCP_CLOSE                    40001
#define ID_TCP_SEND                     40002
#define ID_CLIENT_REMOVE                40003
#define ID_CLIENT_CLEAR                 40004
#define ID_CLIENT_REFRESH               40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
