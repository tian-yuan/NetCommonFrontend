// DialogLog.cpp : implementation file
//

#include "stdafx.h"
#include "DialogLog.h"
#include "DataBase.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogLog dialog


CDialogLog::CDialogLog(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogLog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogLog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDialogLog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogLog)
	DDX_Control(pDX, IDC_LIST_RECV, m_lstLog);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogLog, CDialog)
	//{{AFX_MSG_MAP(CDialogLog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogLog message handlers

BOOL CDialogLog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	DWORD dwNewStyle = LVS_EX_FULLROWSELECT;//|LVS_EX_CHECKBOXES;
	
	m_lstLog.SetExtendedStyle(dwNewStyle);	
	
	m_lstLog.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_lstLog.InsertColumn(0,"时间",LVCFMT_LEFT,70);
	m_lstLog.InsertColumn(1,"地址",LVCFMT_LEFT,120);
	m_lstLog.InsertColumn(2,"端口",LVCFMT_LEFT,60);
	m_lstLog.InsertColumn(3,"数据",LVCFMT_LEFT,320);
	
	_DTU_* pDTU = (_DTU_* )m_pDTU;
	for(POSITION pos = pDTU->m_RecvListt.GetHeadPosition();pos;)
	{
		_RECV__LOG___* pLog = (_RECV__LOG___* )pDTU->m_RecvListt.GetNext(pos);

		int index = m_lstLog.GetItemCount();
		m_lstLog.InsertItem(index,"");

		char str[32] = {0};
		CTime t(pLog->time);
		sprintf(str,"%s",t.Format("%H:%M:%S"));
			
		m_lstLog.SetItemText(index,0,str);
		m_lstLog.SetItemText(index,1,pLog->ip);
		sprintf(str,"%d",pLog->port);
		m_lstLog.SetItemText(index,2,str);

		char strAll[10240] = {0};
		if(pLog->nType == 1)
		{
			sprintf(strAll,"接收 %2d ",pLog->nLen);
		}
		if(pLog->nType == 2)
		{
			sprintf(strAll,"发送 %2d ",pLog->nLen);
		}
		if(pLog->nType == 3)
		{
			sprintf(strAll,"信息 %2d ",pLog->nLen);
		}
		for(int i = 0;i < pLog->nLen;i ++)
		{
			char s[10] = {0};
			sprintf(s,"%02X ",pLog->buff[i]);
			strcat(strAll,s);
		}
		m_lstLog.SetItemText(index,3,strAll);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
