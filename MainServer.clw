; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDialogConfig
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "mainserver.h"
LastPage=0

ClassCount=6
Class1=CDialogConfig
Class2=CDialogDTU
Class3=CDialogMeter
Class4=CDialogText
Class5=CDialogView
Class6=CVirtualListCtrl

ResourceCount=7
Resource1=IDD_DIALOG_TEXT
Resource2=IDD_DIALOG_LOG
Resource3=IDD_DIALOG_METER
Resource4=IDD_DIALOG_DTU
Resource5=IDD_DIALOG_CONFIG
Resource6=IDD_DIALOG_VIEW
Resource7=IDR_MENU_POP

[CLS:CDialogConfig]
Type=0
BaseClass=CDialog
HeaderFile=DialogConfig.h
ImplementationFile=DialogConfig.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_EDIT_TIMEOUT

[CLS:CDialogDTU]
Type=0
BaseClass=CDialog
HeaderFile=DialogDTU.h
ImplementationFile=DialogDTU.cpp

[CLS:CDialogMeter]
Type=0
BaseClass=CDialog
HeaderFile=DialogMeter.h
ImplementationFile=DialogMeter.cpp

[CLS:CDialogText]
Type=0
BaseClass=CDialog
HeaderFile=DialogText.h
ImplementationFile=DialogText.cpp

[CLS:CDialogView]
Type=0
BaseClass=CDialog
HeaderFile=DialogView.h
ImplementationFile=DialogView.cpp
LastObject=CDialogView

[CLS:CVirtualListCtrl]
Type=0
BaseClass=CListCtrl
HeaderFile=VirtualListCtrl.h
ImplementationFile=VirtualListCtrl.cpp

[DLG:IDD_DIALOG_CONFIG]
Type=1
Class=CDialogConfig
ControlCount=16
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDIT_PORT,edit,1350631554
Control6=IDC_EDIT_TIMEOUT,edit,1350631554
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_PORT2,edit,1350631554
Control9=IDC_STATIC,static,1342308352
Control10=IDC_EDIT_TIME,edit,1350631554
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT_LOG,edit,1350631554
Control13=IDC_EDIT_NAME,edit,1350631552
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_EDIT_LINE,edit,1350631554

[DLG:IDD_DIALOG_DTU]
Type=1
Class=CDialogDTU
ControlCount=8
Control1=IDC_BUTTON_REFRESH,button,1342242816
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_TIME_ACTIVE,edit,1350633600
Control4=IDC_LIST_METER_ALL,SysListView32,1350631429
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_NUM,edit,1350633600
Control7=IDC_STATIC,static,1342308352
Control8=IDC_BUTTON_COMM,button,1342242816

[DLG:IDD_DIALOG_METER]
Type=1
Class=CDialogMeter
ControlCount=10
Control1=IDC_BUTTON_REFRESH,button,1342242816
Control2=IDC_BUTTON_TYPE,button,1342242816
Control3=IDC_EDIT_TYPE,edit,1350639744
Control4=IDCANCEL,button,1073807360
Control5=IDC_EDIT_DATA,edit,1352730692
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_TIME_SEND,edit,1350633600
Control9=IDC_EDIT_TIME_RECV,edit,1350633600
Control10=IDC_LIST_METER,SysListView32,1350631429

[DLG:IDD_DIALOG_TEXT]
Type=1
Class=CDialogText
ControlCount=4
Control1=IDC_EDIT_TEXT,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_STATIC,static,1342308352

[DLG:IDD_DIALOG_VIEW]
Type=1
Class=CDialogView
ControlCount=8
Control1=IDCANCEL,button,1342242816
Control2=IDC_LIST_CONNECT,SysListView32,1350668301
Control3=IDC_BUTTON_REG,button,1342242816
Control4=IDC_BUTTON_DEL,button,1342242816
Control5=IDC_BUTTON_CHECK,button,1342242816
Control6=IDC_LIST_WAIT,SysListView32,1350668301
Control7=IDC_BUTTON_CONFIG,button,1342242816
Control8=IDC_BUTTON_LOAD,button,1342242816

[MNU:IDR_MENU_POP]
Type=1
Class=?
Command1=ID_TCP_SEND
Command2=ID_TCP_CLOSE
Command3=ID_CLIENT_REMOVE
Command4=ID_CLIENT_CLEAR
Command5=ID_CLIENT_REFRESH
CommandCount=5

[DLG:IDD_DIALOG_LOG]
Type=1
Class=?
ControlCount=2
Control1=IDCANCEL,button,1073807360
Control2=IDC_LIST_RECV,SysListView32,1350631429

