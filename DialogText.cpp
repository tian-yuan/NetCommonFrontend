// DialogText.cpp : implementation file
//

#include "stdafx.h"
#include "DialogText.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogText dialog


CDialogText::CDialogText(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogText::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogText)
	m_strText = _T("0123456789");
	//}}AFX_DATA_INIT
}


void CDialogText::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogText)
	DDX_Text(pDX, IDC_EDIT_TEXT, m_strText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogText, CDialog)
	//{{AFX_MSG_MAP(CDialogText)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogText message handlers
