// DataBase.cpp: implementation of the CDataBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataBase.h"
#include "mydefine.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#include "DialogConfig.h"


#include "Locker.h"

#include "zip.h"
#include "unzip.h"

#include "Location.h"

int g_nVer = 6;//版本

CTime GetBuildDateTime()
{
	char s_month[5];
	int month, day, year;
	char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
	sscanf(__DATE__, "%s %d %d", s_month, &day, &year);
	month = (strstr(month_names, s_month)-month_names)/3 + 1;
	
	int h,m,s;
	sscanf(__TIME__, "%d:%d:%d",&h,&m,&s);
	
	CTime t(year,month,day,h,m,s);
	
	return t;
}

CDataBase::CDataBase()
{
	WSADATA WSAData;
	WSAStartup(0x101, &WSAData);
	m_serverA = std::make_shared<ConnectManager<MyConnectionA>>(3);
	m_serverB = std::make_shared<ConnectManager<MyConnectionB>>(10);
	char strQQ[MAX_PATH] = {0};
	char strLocation[MAX_PATH] = {0};
	sprintf(strQQ,"%s%s",GetCurrentPathName(),"qqwry.dat");
	CLocation location(strQQ);
	
	location.GetLocation("202.192.156.3",strLocation);

	memset(m_strTitle,0,sizeof(m_strTitle));

//	ZipFile("C:\\MonitorData.mdb");
//	UnZipFile("C:\\bb\\MonitorData.mdb.zip");
	m_ctLockA = CreateMutex(NULL, false, NULL);
	m_ctLockB = CreateMutex(NULL, false, NULL);
	m_ctLockW = CreateMutex(NULL, false, NULL);
	m_ctLockFile = CreateMutex(NULL, false, NULL);

	CString strD;
	strD = "mengHX";
	strD.Format("%s_%s",GetCurrentPathName(),"mengHX");
	strD.Replace(":","");
	strD.Replace("\\","_");
	if(strD.GetLength() > 32)
		m_strDSN = strD.Right(32);
	else
		m_strDSN = strD;

// 	_METER_* pData = new _METER_;
// 
// 	memset(pData,0,sizeof(_METER_));
// 
// 	pData->data.nID = 7;
// 	m_MeterList.AddTail(pData);

	m_uRecvLen = 0;
	m_uSendLen = 0;

	m_lStartTime = CTime::GetCurrentTime().GetTime();

	m_sListenSocketA = 0;//主控监听
	m_sListenSocketB = 0;//分控监听
	
	m_nPortA = 9988;//主控端口 +1 分控端口
	m_nPortB = m_nPortA + 1;//分控端口
	
	m_nTimeOut = 60;//超时时间
	
	m_nCheckTime = 120;//查询时间
	
	m_nLogDay = 7;//最大日志天数

	m_nLogLine = 100;

}

void CDataBase::Release()
{
	POSITION pos;
	for(pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);
		
		for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);
			delete pMeter;
		}
		pDTU->m_MeterList.RemoveAll();

		for(POSITION posT = pDTU->m_RecvListt.GetHeadPosition();posT;)
		{
			_RECV__LOG___* pLog = (_RECV__LOG___ *)pDTU->m_RecvListt.GetNext(posT);
			delete []pLog->buff;
			delete pLog;
		}
		pDTU->m_RecvListt.RemoveAll();

		delete pDTU;
	}
	m_DTUList.RemoveAll();
	
	for(pos = m_ClientList.GetHeadPosition();pos;)
	{
		_CLIENT_* pClient = (_CLIENT_ *)m_ClientList.GetNext(pos);
		delete pClient;
	}
	m_ClientList.RemoveAll();
	if(m_ctLockA > 0)
		CloseHandle(m_ctLockA);
	if(m_ctLockB > 0)
		CloseHandle(m_ctLockB);
	if(m_ctLockW > 0)
		CloseHandle(m_ctLockW);

	m_ctLockA = 0;
	m_ctLockB = 0;
	m_ctLockW = 0;

	m_serverA->stop();
	m_serverB->stop();
	for (int i = 0; i < m_serverThreadPool.size(); ++i) {
		m_serverThreadPool[i].join();
	}
}

CDataBase::~CDataBase()
{
	Release();
	WSACleanup();
}

BOOL CDataBase::RegisterDBSource(CString strDSName, CString strDBPath,CString strSystemPath)
{
	HKEY hKey;
    DWORD nLabel; 
	
	char strBaseKey[MAX_PATH] = {"SOFTWARE\\ODBC\\ODBC.INI"};
	char strMid[MAX_PATH] = {"SOFTWARE\\ODBC\\ODBC.INI"};
	strcat(strMid,"\\ODBC Data Sources");
	
	if(strDSName.IsEmpty()) 
		return FALSE;
	if(strDBPath.IsEmpty()) 
		return FALSE;
	
	char strDataSource[MAX_PATH] = {0};
	sprintf(strDataSource,"%s\\%s",strBaseKey,strDSName);
	
	CString strMdb = _T("Microsoft Access Driver (*.mdb)");
	CString strDBDriver = _T("C:\\WINNT\\System32\\odbcjt32.dll");
	strDBDriver.Format("%s\\odbcjt32.dll",strSystemPath);
	CString strFIL = _T("Ms Access;");
	CString strUID = _T("");

	LONG ret = -1;
	ret = RegCreateKeyEx(HKEY_CURRENT_USER,
		strMid,
        0, 
		NULL, 
		REG_OPTION_NON_VOLATILE, 
		KEY_ALL_ACCESS,
		NULL,
		&hKey, 
		&nLabel );//获取数据源键值句柄
	if(ERROR_SUCCESS != ret)
		return FALSE;

	ret = RegSetValueEx(hKey,
		strDSName,
		0,
		REG_SZ,
		(const unsigned char *)((LPCTSTR)strMdb),
		strlen((LPCTSTR)strMdb)+1);///设置数据源类型
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	ret = RegCreateKeyEx(HKEY_CURRENT_USER,
		strDataSource,
		0, 
		NULL, 
		REG_OPTION_NON_VOLATILE, 
		KEY_ALL_ACCESS,
		NULL,
		&hKey, 
		&nLabel );//创建数据源子键
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	ret = RegSetValueEx(hKey,
		_T("DBQ"),
		0,
		REG_SZ,
		(const unsigned char *)((LPCTSTR)strDBPath),
		strlen((LPCTSTR)strDBPath)+1);//数据库表的全路径
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	ret = RegSetValueEx(hKey,
		_T("Driver"),
		0,
		REG_SZ,
		(const unsigned char *)((LPCTSTR)strDBDriver),
		strlen((LPCTSTR)strDBDriver)+1);//ODBC驱动的全路径
	
	if(ERROR_SUCCESS != ret)
		return FALSE;
	ret = RegSetValueEx(hKey,
		_T("FIL"),
		0,
		REG_SZ,
		(const unsigned char *)((LPCTSTR)strFIL),
		strlen((LPCTSTR)strFIL)+1);//表的类型
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	ret = RegSetValueEx(hKey,
		_T("UID"),
		0,
		REG_SZ,
		(const unsigned char *)((LPCTSTR)strUID),
		strlen((LPCTSTR)strUID)+1);//必须项
	
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	DWORD DriverId = (DWORD)25;
	ret = RegSetValueEx(hKey,
		_T("DriverId"),
		0,
		REG_DWORD,
		(const BYTE *)(&DriverId),
		sizeof(DWORD));//必须项
	
	if(ERROR_SUCCESS != ret)
		return FALSE;
	
	DWORD SafeTrans = (DWORD)0;
	ret = RegSetValueEx(hKey,
		_T("SafeTransactions"),
		0,
		REG_DWORD,
		(const BYTE *)(&SafeTrans),
		sizeof(DWORD));//可选项
	if(ERROR_SUCCESS != ret)
		return FALSE;

	return TRUE;
}

CString CDataBase::GetCurrentPathName()
{
	char buf[MAX_PATH] = {0};
	DWORD len = MAX_PATH;
	
	GetModuleFileName(NULL,buf,len);
	CString exe;
	exe.Format("%s",buf);
	int n = exe.ReverseFind('\\');
	exe = exe.Left(n);
	exe += "\\";
	return exe;
}


BOOL CDataBase::AddODBC()
{
	char name[MAX_PATH] = {0};
	sprintf(name,"%s%s",GetCurrentPathName(),"MonitorData.mdb");
	char sys[MAX_PATH] = {0};
	GetSystemDirectory(sys,MAX_PATH);
	//yst_vip
	return RegisterDBSource(m_strDSN,name,sys);
	
	return TRUE;
}

BOOL CDataBase::InsertRecord(char *pData)
{
	try
	{
		m_dbMain.ExecuteSQL(pData);
		
		return TRUE;
	}
	catch (CDBException* e)
	{
		CString str;
		str.Format("ExecuteSQL err:%s %s",e->m_strError,pData);
		
		OutputDebug(str);
		e->Delete();
	}
	
	return FALSE;
}

BOOL CDataBase::ConnectDB()
{
	CString strSQL;

#ifdef connectSql2008
	strSQL.Format("ODBC;DSN=%s;UID=%s;PWD=%s",
		"mengHX3","sa","xh6565911");
	strSQL.MakeUpper();
#else
	strSQL.Format("ODBC;DSN=%s;UID=%s;PWD=%s",
		m_strDSN, "Admin", "123321");
	strSQL.MakeUpper();

#endif
	
	try
	{
		if(m_dbMain.IsOpen())
			m_dbMain.Close();
		
		m_dbMain.Open(NULL,FALSE,FALSE,strSQL);
		
		LoadBaseTables();

		return TRUE;
	}
	catch (CDBException* e)
	{
		CString str;
		str.Format("连接数据库错误:%s(%s)",e->m_strError,strSQL);
//		AfxMessageBox(str);
		OutputDebug(str);
		e->Delete();
	}
	return FALSE;
}

int CDataBase::ExcuteSQL(CString pSQL,_CLIENT_* pClient)
{
	try
	{
		if(!m_dbMain.IsOpen())
		{
			return 1;
		}
		m_dbMain.ExecuteSQL(pSQL);
		return 0;
	}
	catch (CDBException* e)
	{
		CString str;
		str.Format("2 执行数据库错误:%s(%s)",e->m_strError,pSQL);
		
		OutputDebug(str);
		e->Delete();

		return 2;
	}
}

_DTU_* CDataBase::FindDTU(char* pDTU_ID)
{
	char strD[128] = { 0 };
	strcpy(strD, pDTU_ID);
	
	for (POSITION pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);

		if(stricmp(strD,pDTU->str_ID) == 0)
		

			return pDTU;
	}
	return NULL;
}
#ifdef addEncryptionCode
_DTU_* CDataBase::FindDTU2(char* pDTU_ID)
{
	char mystr_ID[40];//数据库存放
	for (POSITION pos = m_DTUList.GetHeadPosition(); pos;)
	{
		_DTU_* pDTU = (_DTU_*)m_DTUList.GetNext(pos);

		memcpy(mystr_ID, pDTU->str_ID, 40);
		/*
		OutputDebug("收到分控 77777777 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[0], mystr_ID[1], mystr_ID[2], mystr_ID[3], mystr_ID[4], mystr_ID[5], mystr_ID[6], mystr_ID[7], mystr_ID[8], mystr_ID[9]);
		OutputDebug("收到分控 8888888888 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[10], mystr_ID[11], mystr_ID[12], mystr_ID[13], mystr_ID[14], mystr_ID[15], mystr_ID[16], mystr_ID[17], mystr_ID[18], mystr_ID[19]);
		OutputDebug("收到分控 999999 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[20], mystr_ID[21], mystr_ID[22], mystr_ID[23], mystr_ID[24], mystr_ID[25], mystr_ID[26], mystr_ID[27], mystr_ID[28], mystr_ID[29]);
		OutputDebug("收到分控 555555 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[30], mystr_ID[31], mystr_ID[32], mystr_ID[33], mystr_ID[34], mystr_ID[35], mystr_ID[36], mystr_ID[37], mystr_ID[38], mystr_ID[39]);
		*/
		memset(&mystr_ID[16], 0x30, 10); //把加密码清成0X30也就是“0”
								 /*
		OutputDebug("收到分控bb 77777777 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[0], mystr_ID[1], mystr_ID[2], mystr_ID[3], mystr_ID[4], mystr_ID[5], mystr_ID[6], mystr_ID[7], mystr_ID[8], mystr_ID[9]);
		OutputDebug("收到分控bb 8888888888 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[10], mystr_ID[11], mystr_ID[12], mystr_ID[13], mystr_ID[14], mystr_ID[15], mystr_ID[16], mystr_ID[17], mystr_ID[18], mystr_ID[19]);
		OutputDebug("收到分控bb 999999 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[20], mystr_ID[21], mystr_ID[22], mystr_ID[23], mystr_ID[24], mystr_ID[25], mystr_ID[26], mystr_ID[27], mystr_ID[28], mystr_ID[29]);
		OutputDebug("收到分控bb 555555 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", mystr_ID[30], mystr_ID[31], mystr_ID[32], mystr_ID[33], mystr_ID[34], mystr_ID[35], mystr_ID[36], mystr_ID[37], mystr_ID[38], mystr_ID[39]);
		
		OutputDebug("收到分控AA 77777777 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", pDTU_ID[0], pDTU_ID[1], pDTU_ID[2], pDTU_ID[3], pDTU_ID[4], pDTU_ID[5], pDTU_ID[6], pDTU_ID[7], pDTU_ID[8], pDTU_ID[9]);
		OutputDebug("收到分控AA 8888888888 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", pDTU_ID[10], pDTU_ID[11], pDTU_ID[12], pDTU_ID[13], pDTU_ID[14], pDTU_ID[15], pDTU_ID[16], pDTU_ID[17], pDTU_ID[18], pDTU_ID[19]);
		OutputDebug("收到分控AA 999999 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", pDTU_ID[20], pDTU_ID[21], pDTU_ID[22], pDTU_ID[23], pDTU_ID[24], pDTU_ID[25], pDTU_ID[26], pDTU_ID[27], pDTU_ID[28], pDTU_ID[29]);
		OutputDebug("收到分控AA 555555 mystr_ID: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d", pDTU_ID[30], pDTU_ID[31], pDTU_ID[32], pDTU_ID[33], pDTU_ID[34], pDTU_ID[35], pDTU_ID[36], pDTU_ID[37], pDTU_ID[38], pDTU_ID[39]);
		*/
		//	OutputDebug("收到分控  w  mystr_ID: %d,%d", mystr_ID[16], mystr_ID[17]);
		if (stricmp(pDTU_ID, mystr_ID) == 0)
		{
		//	OutputDebug("find it");
			return pDTU;
		}
		else
		{
		//	OutputDebug("no find it");
		}
	}
	return NULL;
}
#endif			


BOOL CDataBase::CreateTable(char* pSQL)//创建表
{
	if(!m_dbMain.IsOpen())
		return FALSE;
	try
	{
		m_dbMain.ExecuteSQL(pSQL);
	}
	catch (CDBException* e)
	{
		char str[1024] = {0};
		sprintf(str,"db:%s %s",e->m_strError,pSQL);
		
//		AfxMessageBox(str);
		e->Delete();
		return FALSE;
	}
	return TRUE;
}

void CDataBase::LoadBaseTables2()
{
	if(!m_dbMain.IsOpen())
		return;

	{
		CDynamicBulkSet rs(&m_dbMain);
		
		char strSQL[1024] = {0};
		
		sprintf(strSQL,"select DTU_ID,WaterWellID,type from WaterWellDevice");// where Status = %d ,1
		
		try
		{
			int nDTU = 0;
			int nMeter = 0;
			rs.Open(strSQL);
			
			CODBCFieldInfo info;
			int nColumns = rs.GetODBCFieldCount();
			
			int n = rs.GetRecordCount();
			if (rs.IsEOF() && rs.IsBOF())
			{
				rs.Close();
				return;
			}
			
			int nRowsFetched = rs.GetRowsFetched();
			
			if(nRowsFetched > 50000)
			{
				rs.Close();
				
				return;
			}
			
			for (int nRow = 0; nRow < nRowsFetched; nRow++)
			{
				char strAll[3][100] = {0,0};
				for (int nField = 0; nField < nColumns; nField++)
				{
					long* rgLength;
					LPSTR rgData;
					rgData = (LPSTR)rs.m_ppvData[nField];
					rgLength = (long*)rs.m_ppvLengths[nField];
					int nStatus = rs.GetRowStatus(nRow + 1);
					
					CString strData;
					if (nStatus == SQL_ROW_DELETED)
						strData = _T("");
					else if (nStatus == SQL_ROW_NOROW)
						strData = _T(" ");
					else if (rgLength[nRow] == SQL_NULL_DATA)
						strData = _T(" ");
					else
						strData = &rgData[nRow * MAX_TEXT_LEN];
					strData.TrimLeft();
					strData.TrimRight();
					strcpy(strAll[nField],strData);
				}
			//	OutputDebug("DTU = %s   ID = %s  Type = %s",strAll[0],strAll[1],strAll[2]);
				//DTU ID
				_DTU_* pDTU = FindDTU(strAll[0]);
				if(!pDTU)
				{
					pDTU = new _DTU_;
					memset(pDTU->ip,0,sizeof(pDTU->ip));
					memset(pDTU->port,0,sizeof(pDTU->port));
					memset(pDTU->m_strLastRecv,0,sizeof(pDTU->m_strLastRecv));

					pDTU->sSocket = 0;
					pDTU->dwConnectTime = 0;
					pDTU->dwSendDataTime = 0;
					pDTU->dwRecvActiveTime = 0;
					pDTU->m_nLastLen = 0;

					strcpy(pDTU->str_ID,strAll[0]);
					m_DTUList.AddTail(pDTU);
					nDTU ++;
				}
				CString s(strAll[0]);
				_METER_* pOld = FindMeter(pDTU,atoi(strAll[1]));
				if(pOld)
				{
					_METER_* pMeter = pOld;

					if(s.GetLength() > 4)
					{
#ifdef moreThan9999
						pMeter->nDTU_ID = atoi(s.Right(6));
#else
						pMeter->nDTU_ID = atoi(s.Right(4));
#endif
						
					}
					pMeter->nID = atoi(strAll[1]);
					pMeter->m_nType = atoi(strAll[2]);
					if(pMeter->m_nType == 1)
					{
						pMeter->data1.nID = atoi(strAll[1]);
						pMeter->data1.nDTU_ID = atoi(s.Right(4));
						
						pMeter->data1.nType1 = pMeter->m_nType;
						pMeter->data1.lTime = 0;
					}
					else if(pMeter->m_nType == 2)
					{
						pMeter->data2.nID2 = atoi(strAll[1]);
						pMeter->data2.nDTU_ID2 = atoi(s.Right(4));
						
						pMeter->data2.nType2 = pMeter->m_nType;
						pMeter->data2.lTime = 0;
						
					}
					else if(pMeter->m_nType == 3)
					{
						pMeter->data3.nID3 = atoi(strAll[1]);
						pMeter->data3.nDTU_ID3 = atoi(s.Right(4));
						
						pMeter->data3.nType3 = pMeter->m_nType;
						pMeter->data3.lTime = 0;
						
					}
						//pDTU->m_MeterList.AddTail(pMeter);
						//nMeter ++;
				}
				else
				{
					_METER_* pMeter = new _METER_;
					pMeter->dwRecvMeterDataTime = 0;
					memset(&pMeter->data1,0,sizeof(_DATA_METER_1));
					memset(&pMeter->data2,0,sizeof(_DATA_METER_2));
					memset(&pMeter->data3,0,sizeof(_DATA_RTU));
					
					memset(&pMeter->recv_data,0,sizeof(pMeter->recv_data));
					pMeter->pDTU = pDTU;
					if(s.GetLength() > 4)
					{
#ifdef moreThan9999
						pMeter->nDTU_ID = atoi(s.Right(6));
#else
						pMeter->nDTU_ID = atoi(s.Right(4));
#endif

					}
					pMeter->nID = atoi(strAll[1]);
					pMeter->m_nType = atoi(strAll[2]);
					if(pMeter->m_nType == 1)
					{
						pMeter->data1.nID = atoi(strAll[1]);
						pMeter->data1.nDTU_ID = atoi(s.Right(4));
						
						pMeter->data1.nType1 = pMeter->m_nType;
						pMeter->data1.lTime = 0;
					}
					else if(pMeter->m_nType == 2)
					{
						pMeter->data2.nID2 = atoi(strAll[1]);
						pMeter->data2.nDTU_ID2 = atoi(s.Right(4));
						
						pMeter->data2.nType2 = pMeter->m_nType;
						pMeter->data2.lTime = 0;
						
					}
					else if(pMeter->m_nType == 3)
					{
						pMeter->data3.nID3 = atoi(strAll[1]);
						pMeter->data3.nDTU_ID3 = atoi(s.Right(4));
						
						pMeter->data3.nType3 = pMeter->m_nType;
						pMeter->data3.lTime = 0;
						
					}
					pMeter->dwSendMeterDataTime = GetTime() - 10 + pMeter->nID;

					pDTU->m_MeterList.AddTail(pMeter);
					nMeter ++;
				}

			}
			
			rs.Close();

			OutputDebug("加载数据库 DTU = %d Meter = %d.",nDTU,nMeter);
		}
		catch (CDBException* e)
		{
			char str[1024] = {0};
			sprintf(str,"db:%s %s",e->m_strError,strSQL);
			
			//	AfxMessageBox(str);
			e->Delete();
			return ;
		}
	}


	//检查旧的是否可以删除
	{
		POSITION pos;
		for(pos = m_DTUList.GetHeadPosition();pos;)
		{
			POSITION pos2 = pos;
			_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);
			
			for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
			{
				POSITION pos3 = posT;
				_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);
				
				if(!CheckIDfromDatabase(pDTU->str_ID,pMeter->nID))
				{
					delete pMeter;
					pDTU->m_MeterList.RemoveAt(pos3);
				}
			}

			if(pDTU->m_MeterList.GetCount() <= 0)
			{
				for(POSITION posT = pDTU->m_RecvListt.GetHeadPosition();posT;)
				{
					_RECV__LOG___* pLog = (_RECV__LOG___ *)pDTU->m_RecvListt.GetNext(posT);
					delete []pLog->buff;
					delete pLog;
				}
				pDTU->m_RecvListt.RemoveAll();

				delete pDTU;
				m_DTUList.RemoveAt(pos2);
			}
		}

	}
}

BOOL CDataBase::CheckIDfromDatabase(char* pDTU_ID,int nMeterID)
{
	CDynamicBulkSet rs(&m_dbMain);
	
	char strSQL[1024] = {0};
	
	sprintf(strSQL,"select count(*) from WaterWellDevice where DTU_ID = \'%s\' and WaterWellID = %d",
		pDTU_ID,nMeterID);// where Status = %d ,1
	
	try
	{
		rs.Open(strSQL);
		
		int nRowsFetched = rs.GetRowsFetched();
		
		if(nRowsFetched > 0)
		{
			LPSTR rgData;
			rgData = (LPSTR)rs.m_ppvData[0];

			if(atoi(rgData) > 0)
			{
				rs.Close();
				
				return TRUE;
			}
		}
		
		rs.Close();
		return FALSE;
	}
	catch (CDBException* e)
	{
		char str[1024] = {0};
		sprintf(str,"db:%s %s",e->m_strError,strSQL);
		
		//	AfxMessageBox(str);
		e->Delete();
		return TRUE;
	}
	return TRUE;
}

BOOL CDataBase::CheckTable(char* pTableName)
{
	if(!m_dbMain.IsOpen())
		return FALSE;

	CDynamicBulkSet rs(&m_dbMain);
	
	char strSQL[1024] = {0};
	
	sprintf(strSQL,"select count(*) from %s ",pTableName);
	
	try
	{
		rs.Open(strSQL);
		
		int nRowsFetched = rs.GetRowsFetched();
		
		if(nRowsFetched > 0)
		{
			rs.Close();
			
			return TRUE;
		}
	}
	catch (CDBException* e)
	{
		char str[1024] = {0};
		sprintf(str,"db:%s %s",e->m_strError,strSQL);
		
		//	AfxMessageBox(str);
		e->Delete();
		return FALSE;
	}
	return FALSE;
}

void CDataBase::LoadBaseTables()
{
	if(!m_dbMain.IsOpen())
		return;
	
	POSITION pos;
	for(pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);
		
		for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);
			delete pMeter;
		}
		pDTU->m_MeterList.RemoveAll();

		for(POSITION posT = pDTU->m_RecvListt.GetHeadPosition();posT;)
		{
			_RECV__LOG___* pLog = (_RECV__LOG___ *)pDTU->m_RecvListt.GetNext(posT);
			delete []pLog->buff;
			delete pLog;
		}
		pDTU->m_RecvListt.RemoveAll();

	}

	CDynamicBulkSet rs(&m_dbMain);
	
	char strSQL[1024] = {0};
	
	sprintf(strSQL,"select DTU_ID,WaterWellID,type from WaterWellDevice");// where Status = %d ,1
	
	try
	{
		int nDTU = 0;
		int nMeter = 0;
		rs.Open(strSQL);
		
		CODBCFieldInfo info;
		int nColumns = rs.GetODBCFieldCount();
		
		int n = rs.GetRecordCount();
		if (rs.IsEOF() && rs.IsBOF())
		{
			rs.Close();
			return;
		}
		
		int nRowsFetched = rs.GetRowsFetched();
		
		if(nRowsFetched >50000)
		{
			rs.Close();
			
			return;
		}
		
		for (int nRow = 0; nRow < nRowsFetched; nRow++)
		{
			char strAll[3][100] = {0,0};
			for (int nField = 0; nField < nColumns; nField++)
			{
				long* rgLength;
				LPSTR rgData;
				rgData = (LPSTR)rs.m_ppvData[nField];
				rgLength = (long*)rs.m_ppvLengths[nField];
				int nStatus = rs.GetRowStatus(nRow + 1);
				
				CString strData;
				if (nStatus == SQL_ROW_DELETED)
					strData = _T("");
				else if (nStatus == SQL_ROW_NOROW)
					strData = _T(" ");
				else if (rgLength[nRow] == SQL_NULL_DATA)
					strData = _T(" ");
				else
					strData = &rgData[nRow * MAX_TEXT_LEN];
				strData.TrimLeft();
				strData.TrimRight();
				strcpy(strAll[nField],strData);
			}
			OutputDebug("DTU = %s   ID = %s  Type = %s",strAll[0],strAll[1],strAll[2]);
			//DTU ID
			_DTU_* pDTU = FindDTU(strAll[0]);
			if(!pDTU)
			{
				pDTU = new _DTU_;
				memset(pDTU->ip,0,sizeof(pDTU->ip));
				memset(pDTU->port,0,sizeof(pDTU->port));
				memset(pDTU->m_strLastRecv,0,sizeof(pDTU->m_strLastRecv));

				pDTU->sSocket = 0;
				pDTU->dwConnectTime = 0;
				pDTU->dwSendDataTime = 0;
				pDTU->dwRecvActiveTime = 0;
				pDTU->m_nLastLen = 0;

				strcpy(pDTU->str_ID,strAll[0]);
				m_DTUList.AddTail(pDTU);
				nDTU ++;
			}
			CString s(strAll[0]);
			_METER_* pMeter = new _METER_;
			pMeter->dwRecvMeterDataTime = 0;
			pMeter->dwSendMeterDataTime = 0;
			memset(&pMeter->data1,0,sizeof(_DATA_METER_1));
			memset(&pMeter->data2,0,sizeof(_DATA_METER_2));
			memset(&pMeter->data3,0,sizeof(_DATA_RTU));

			memset(&pMeter->recv_data,0,sizeof(pMeter->recv_data));
			pMeter->pDTU = pDTU;
			if(s.GetLength() > 4)
			{
#ifdef moreThan9999
				pMeter->nDTU_ID = atoi(s.Right(6));
#else
				pMeter->nDTU_ID = atoi(s.Right(4));
#endif

			}
			pMeter->nID = atoi(strAll[1]);
			pMeter->m_nType = atoi(strAll[2]);
			if(pMeter->m_nType == 1)
			{
				pMeter->data1.nID = atoi(strAll[1]);
				pMeter->data1.nDTU_ID = atoi(s.Right(4));

				pMeter->data1.nType1 = pMeter->m_nType;
				pMeter->data1.lTime = 0;
			}
			else if(pMeter->m_nType == 2)
			{
				pMeter->data2.nID2 = atoi(strAll[1]);
				pMeter->data2.nDTU_ID2 = atoi(s.Right(4));

				pMeter->data2.nType2 = pMeter->m_nType;
				pMeter->data2.lTime = 0;

			}
			else if(pMeter->m_nType == 3)
			{
				pMeter->data3.nID3 = atoi(strAll[1]);
				pMeter->data3.nDTU_ID3 = atoi(s.Right(4));

				pMeter->data3.nType3 = pMeter->m_nType;
				pMeter->data3.lTime = 0;

			}
			pMeter->dwSendMeterDataTime = GetTime() - 10 + pMeter->nID;
			pDTU->m_MeterList.AddTail(pMeter);
			nMeter ++;
		}
		
		rs.Close();

		OutputDebug("加载数据库 DTU = %d Meter = %d.",nDTU,nMeter);
	}
	catch (CDBException* e)
	{
		char str[1024] = {0};
		sprintf(str,"db:%s %s",e->m_strError,strSQL);
		
		//	AfxMessageBox(str);
		e->Delete();
		return ;
	}
}


CString CDataBase::ReadString(LPCTSTR appname, LPCTSTR keyname,LPCTSTR default_value, LPCTSTR file_name)
{
	char str[100];
	GetPrivateProfileString(appname,keyname,default_value,str,255,file_name);
	
	return (CString)str;
}

int CDataBase::ReadInt(LPCTSTR appname, LPCTSTR keyname,int default_value, LPCTSTR file_name)
{
	return GetPrivateProfileInt(appname,keyname,default_value,file_name);
}

void CDataBase::WriteString(LPCTSTR appname, LPCTSTR keyname, LPCTSTR text, LPCTSTR file_name)
{
	WritePrivateProfileString(appname,keyname,text,file_name);
}

void CDataBase::WriteInt(LPCTSTR appname, LPCTSTR keyname, int value, LPCTSTR file_name)
{
	char r[10];
	_itoa(value,r,10);
	
	WritePrivateProfileString(appname,keyname,r,file_name);
}

CString CDataBase::GetInitFile()
{
	CString strFileName;
	
	strFileName.Format("%sSystem.ini",GetCurrentPathName());
	
	return strFileName;
}

CString CDataBase::GetTimeString(DWORD dwTime,BOOL bAll)
{
	if(dwTime == 0)
		return "";
	CTime t(dwTime);
	if(dwTime == -1)
		t = CTime::GetCurrentTime();
	if(!bAll)
		return t.Format("%H:%M:%S");
	else
		return t.Format("%Y-%m-%d %H:%M:%S");
}

long CDataBase::GetTime()
{
	CTime t;
	t = CTime::GetCurrentTime();
	return t.GetTime();
}


CString CDataBase::GetThisFileName()
{
	char buf[MAX_PATH] = {0};
	DWORD len = MAX_PATH;
	
	GetModuleFileName(NULL,buf,len);
	CString exe;
	exe.Format("%s",buf);
	return exe;
}

WORD CDataBase::Caluation_CRC16(BYTE Buff[], int nSize)
{
	WORD nOld;
	WORD nRet = 0xffff;
	unsigned short i,j;
	for(i=0; i<nSize; i++)
	{
		nRet ^= Buff[i];
		for(j=0; j<8; j++)
		{
			nOld = nRet;
			nRet >>= 1;
			if(nOld&0x0001)
				nRet ^= 0xa001;
		}
	}
	return nRet;
}

_METER_* CDataBase::FindMeter(_DTU_* pDTU,int nID)
{
	POSITION pos;
	for(pos = pDTU->m_MeterList.GetHeadPosition();pos;)
	{
		_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(pos);

		if(pMeter->nID == nID)
			return pMeter;
	}
	return NULL;
}

_METER_* CDataBase::FindMeter(int nID)
{
	POSITION pos;
	for(pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);
		
		for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);

			if(pMeter->nID == nID)
				return pMeter;
		}
	}
	return NULL;
}

void CDataBase::SendToAllClient(BYTE* buff,int nLen,_DTU_* pDTU,BOOL bSendData)
{
	WORD dddd = Caluation_CRC16((BYTE* )buff,nLen - 3);
	BYTE bb[2] = {0};
	memcpy(bb,&dddd,2);
	
	if(bb[0] == buff[nLen - 3] && bb[1] == buff[nLen - 2])
	{
		//check ok
		_METER_* pMeter = FindMeter(pDTU,buff[1]);
		if(!pMeter)
		{
			return;
		}
		memcpy(pMeter->recv_data,buff,32);
		pMeter->dwRecvMeterDataTime = GetTime();

//		InsertCommData(pMeter->nDTU_ID,buff,nLen);
		if(bSendData)
		{
			long l = GetTime();
			char strSend[1024] = {0};
			int nType = 0x69;
			memcpy(&strSend[0],&nType,4);
			int len = nLen + 12;
			memcpy(&strSend[4],&len,4);
			memcpy(&strSend[8],&pMeter->nDTU_ID,4);
			memcpy(&strSend[12],&pMeter->m_nType,4);
			memcpy(&strSend[16],&l,4);
			memcpy(&strSend[20],buff,nLen);

			for(POSITION pos = m_ClientList.GetHeadPosition();pos;)
			{
				POSITION posT = pos;
				_CLIENT_* pClient = (_CLIENT_ *)m_ClientList.GetNext(pos);
				
				if(pClient->sSocket > 0 && pClient->nSocketType == 1)
				{
					pClient->dwSendTime = GetTime();
					if(Tcp_Send(NULL,pClient->sSocket,strSend,len + 8) < 0)
					{
						//m_ClientList.RemoveAt(posT);
						closesocket(pClient->sSocket);
						pClient->sSocket = 0;
					}
					else
					{
//						OutputDebug("发送 Meter ID = %d",buff[1]);
						WriteLog(0,"向客户端发送 %s:%s 长度 %d (%s)",pClient->ip,pClient->port,len + 8,Byte2String((BYTE* )strSend,len + 8));
					}
				}
			}
		}
		if(pMeter->m_nType == 1)
		{
			_DATA_METER_1* pData = &pMeter->data1;
			
			int d1 = 0,d2 = 0;
			
			pData->nID = buff[1];
			
			d1 = buff[5],d2 = buff[6],pData->nUedE = d1 * 256 + d2;
			pData->nUedE_E = buff[7];
			
			d1 = buff[8],d2 = buff[9],pData->nLeftE = d1 * 256 + d2;
			pData->nLeftE_E = buff[10];
			
			d1 = buff[11],d2 = buff[12],pData->nUsedM = d1 * 256 + d2;
			pData->nUsedM_E = buff[13];
			
			d1 = buff[14],d2 = buff[15],pData->nLeftM = d1 * 256 + d2;
			pData->nLeftM_E = buff[16];
			
			d1 = buff[17],d2 = buff[18],pData->nRate = d1 * 256 + d2;
			pData->nRate_E = buff[19];
			
			d1 = buff[20],d2 = buff[21],pData->nFlow = d1 * 256 + d2;
			pData->nFlow_E = buff[22];
			
			d1 = buff[23],d2 = buff[24],pData->nFlow2 = d1 * 256 + d2;
			pData->nFlow2_E = buff[25];
			
			d1 = buff[26],d2 = buff[27],pData->nUID = d1 * 256 + d2;
			pData->nUID_E = buff[28];
			
			pData->lTime = GetTime();
		}
		else if(pMeter->m_nType == 2)
		{
			_DATA_METER_2* pData = &pMeter->data2;
			
			int d1 = 0,d2 = 0,d3 = 0;
			
			pData->nID2 = buff[1];
			
			d1 = buff[5],d2 = buff[6],d3 = buff[7],pData->nUedE2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[8],d2 = buff[9],d3 = buff[10],pData->nLeftE2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[11],d2 = buff[12],d3 = buff[13],pData->nUsedM2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[14],d2 = buff[15],d3 = buff[16],pData->nLeftM2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[17],d2 = buff[18],d3 = buff[19],pData->nRate2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[20],d2 = buff[21],d3 = buff[22],pData->nFlowTotal2 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[23],d2 = buff[24],d3 = buff[25],pData->nFlow22 = (d1 * 256 + d2) * 256 + d3;
			
			d1 = buff[26],d2 = buff[27],d3 = buff[28],pData->nUID2 = (d1 * 256 + d2) * 256 + d3;
			
			pData->lTime = GetTime();
		}
		else if(pMeter->m_nType == 3)
		{
			_DATA_RTU* pData = &pMeter->data3;
			
			int d1 = 0,d2 = 0;
			
			pData->nID3 = buff[1];
			
			d1 = buff[5],d2 = buff[6],pData->nFlow = d1 * 256 + d2;
			pData->nFlow_E = buff[7];
			
			d1 = buff[8],d2 = buff[9],pData->nWater = d1 * 256 + d2;
			pData->nWater_E = buff[10];
			
			d1 = buff[11],d2 = buff[12],pData->nTempr = d1 * 256 + d2;
			pData->nTempr_E = buff[13];
			
			d1 = buff[14],d2 = buff[15],pData->nHumidity = d1 * 256 + d2;
			pData->nHumidity_E = buff[16];
			
			d1 = buff[17],d2 = buff[18],pData->nOther1 = d1 * 256 + d2;
			pData->nOther1_E = buff[19];
			
			d1 = buff[20],d2 = buff[21],pData->nOther2 = d1 * 256 + d2;
			pData->nOther2_E = buff[22];
			
			d1 = buff[23],d2 = buff[24],pData->nOther3 = d1 * 256 + d2;
			pData->nOther3_E = buff[25];
			
			d1 = buff[26],d2 = buff[27],pData->nOther4 = d1 * 256 + d2;
			pData->nOther4_E = buff[28];
			
			pData->lTime = GetTime();		
		}
	}//检验
}

void CDataBase::QueryDevice(_METER_* pMeter)
{
	if(pMeter->pDTU->sSocket <= 0)
		return;

	pMeter->dwSendMeterDataTime = GetTime();
	BYTE buff[100] = {0};
	buff[0] = 0x68;//查询设备
	buff[1] = pMeter->nID;
	buff[2] = 0x9;
	buff[3] = 0x3;
	buff[4] = 0x0;
	buff[5] = 0x0;
	
	WORD dddd = Caluation_CRC16(buff,6);
	BYTE bb[2] = {0};
	memcpy(bb,&dddd,2);
	
	buff[6] = bb[0];//0x4E
	buff[7] = bb[1];//0xAF;
	
	buff[8] = 0x16;
	
	int d = Tcp_Send(pMeter->pDTU,pMeter->pDTU->sSocket,(char* )buff,9);
	if(d < 0)
	{
		closesocket(pMeter->pDTU->sSocket);
		pMeter->pDTU->sSocket = 0;
	}
	else
	{
		OutputDebug("查询主控发送 %d",buff[1]);
	}
}

void CDataBase::QueryDevice(_DTU_* pDTU)
{
	if(pDTU->sSocket <= 0)
		return;

	POSITION pos;
	DWORD ttt = GetTime();
	for(pos = pDTU->m_MeterList.GetHeadPosition();pos;)
	{
		_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(pos);

		int end = m_nCheckTime + pMeter->nID + pMeter->dwSendMeterDataTime;
		if(ttt < end)
			continue;

		if(pMeter->m_nType == 1)		//68 ? 09 03 00 00 ** ** 16   
		{
			QueryDevice(pMeter);
		}
		else if(pMeter->m_nType == 2)	//68 ? 09 03 00 00 ** ** 16   
		{
			QueryDevice(pMeter);
		}
		else if(pMeter->m_nType == 3)	//68 ? 09 03 00 00 ** ** 16   
		{
			QueryDevice(pMeter);
		}
	}
	pDTU->dwSendDataTime = GetTime();
}

BOOL CDataBase::SendDtuToAllClient(_DTU_* pDTU)
{
	POSITION pos;
	char buff[100] = {0};
	int type = 0x67;  //首字节发生了改变，从0X68改为0X67，让上位机接收处理

	int len = 12;
	int status = 1;
	long t = CTime::GetCurrentTime().GetTime();
	CString s(pDTU->str_ID);
	int id = atoi(s.Right(4));

	memcpy(&buff[0],&type,4);
	memcpy(&buff[4],&len,4);
	memcpy(&buff[8],&id,4);
	memcpy(&buff[12],&status,4);
	memcpy(&buff[16],&t,4);
	
	for(pos = m_ClientList.GetHeadPosition();pos;)
	{
		_CLIENT_* pClient = (_CLIENT_ *)m_ClientList.GetNext(pos);
		if(pClient->nSocketType == 1)
		{
			Tcp_Send(NULL,pClient->sSocket,buff,20,1);
			pClient->dwSendTime = GetTime();
			
//			OutputDebug("发送DTU = %d status = %d time = %d",id,status,t);
		}
	}
	
	return TRUE;
}

BOOL CDataBase::SendAllMeterToClient(_CLIENT_* pClient)
{
	POSITION pos;
	int nSendNum = 0;
	for(pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )m_DTUList.GetNext(pos);
		
		for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);

			int nLen = 0;
			int nType = 0x68;
			
			char buff[1024] = {0};
			memcpy(&buff[0],&nType,4);
			if(pMeter->m_nType == 1)
			{
				nLen = sizeof(_DATA_METER_1);
				memcpy(&buff[8],&pMeter->data1,nLen);

				OutputDebug("1 Send ======= %d",pMeter->data1.nID);
			}
			else if(pMeter->m_nType == 2)
			{
				nLen = sizeof(_DATA_METER_2);
				memcpy(&buff[8],&pMeter->data2,nLen);

				OutputDebug("2 Send ======= %d",pMeter->data2.nID2);
			}
			else if(pMeter->m_nType == 3)
			{
				nLen = sizeof(_DATA_RTU);
				memcpy(&buff[8],&pMeter->data3,nLen);

				OutputDebug("3 Send ======= %d",pMeter->data3.nID3);

			}
			memcpy(&buff[4],&nLen,4);
			
			int d = Tcp_Send(NULL,pClient->sSocket,buff,nLen + 8);
			nSendNum ++;
			if(d < 0)
			{
				closesocket(pClient->sSocket);
				pClient->sSocket = 0;
				return FALSE;
			}
			else
			{
				pClient->dwSendTime = GetTime();

				WriteLog(0,"向分控端发送 %s:%s 长度 %d (%s)",pClient->ip,pClient->port,nLen + 8,Byte2String((BYTE* )buff,nLen + 8));
			}
		}
	}

	OutputDebug("Send =====num== %d",nSendNum);
	return TRUE;
}

_METER_* CDataBase::getMeterFromList(int nMeterID) {
	CLocker lock(m_ctLockB);
	POSITION pos;
	for (pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_*)m_DTUList.GetNext(pos);

		for (POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);

			if (pMeter->nID == nMeterID)
			{
				return pMeter;
			}
		}
	}
	return NULL;
}

_DTU_* CDataBase::getDtuFromList(int fd) {
	CLocker lock(m_ctLockA);
	POSITION pos, posT;
	//所有的DTU列表  已经验证过的DTU ID
	for (pos = m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_*)m_DTUList.GetNext(pos);

		if (pDTU->sSocket == fd)
			return pDTU;
	}
	return NULL;
}

BOOL CDataBase::ZipFile(CString strFile)
{
	char strNewName[MAX_PATH] = {0},strName[MAX_PATH] = {0};
	strcpy(strName,strFile);

	sprintf(strNewName,"%s.zip",strName);
	HZIP hz = CreateZip(strNewName,0);
	
	int n = strFile.ReverseFind('\\');

	CString name = strFile.Right(strFile.GetLength() - n - 1);
	
	char FileName[MAX_PATH] = {0};
	sprintf(FileName,"%s",name);
	
	DWORD d = ZipAdd(hz,FileName,strName);
	
	CloseZip(hz);
	if(d == 0)
		return TRUE;

	return FALSE;
}

BOOL CDataBase::UnZipFile(CString strFile)
{
	char strFileName[MAX_PATH] = {0};
	strcpy(strFileName,strFile);

	char path[MAX_PATH] = {0};

	int n = strFile.ReverseFind('\\');
	sprintf(path,"%s",strFile.Left(n));
//	SetCurrentDirectory(path);
	
	char strDestName[MAX_PATH] = {0};
	HZIP hz = OpenZip(strFileName,0);
	ZIPENTRY ze; 
	GetZipItem(hz,-1,&ze); 
	int numitems=ze.index;
	for (int i=0; i<numitems; i++)
	{
		GetZipItem(hz,i,&ze);
		sprintf(strDestName,"%s\\%s",path,ze.name);

		UnzipItem(hz,i,strDestName);
	}
	
	CloseZip(hz);

	return TRUE;
}

void CDataBase::WriteLog(int nType,const char * fmt, ...)
{
	return;
    char szData[512] = {0};
    char data[512] = {0};
    memset(data,0,sizeof(data));
	
    va_list args;
	va_start(args, fmt);
	_vsnprintf(szData, sizeof(szData) - 1, fmt, args);
	va_end(args);
	

	SYSTEMTIME sys;
 	GetLocalTime(&sys);
// 	
// 	sprintf(data,"%s %3d %s\n",CTime::GetCurrentTime().Format("%H:%M:%S "),sys.wMinute,szData);
	
	char strTime[100] = {0},strTime2[100] = {0};
	sprintf(strTime,"%d-%d-%d %d:%d:%d",sys.wYear,sys.wMonth,sys.wDay,sys.wHour,sys.wMinute,sys.wSecond);
	sprintf(strTime2,"%04d_%02d_%02d",sys.wYear,sys.wMonth,sys.wDay);

	char strTableName[100] = {0};
	sprintf(strTableName,"CommLog%s",strTime2);
//	strTime = CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S");
	char strSQL[1024] = {0};
	sprintf(strSQL,"Insert into %s (LogTime,logType,LogText) values (#%s#,%d,\'%s\')",
		strTableName,strTime,nType,szData);

	try
	{
		if(!CheckTable(strTableName))
		{
			char strSQL[1024] = {0};
			sprintf(strSQL,"CREATE TABLE %s (ID autoincrement,LogTime DATETIME,LogType int,LogText CHAR)",strTableName);
			CreateTable(strSQL);

			//删除以前的日志	
			CTime tCu = CTime::GetCurrentTime();
			int nTime = 24 * 3688 * (m_nLogDay + 1);
			CTime tBefore(tCu.GetTime() - nTime);

			sprintf(strTableName,"CommLog%s",tBefore.Format("%Y_%m_%d"));
			sprintf(strSQL,"delete from %s ",strTableName);
			ExcuteSQL(strSQL);

		}

		m_dbMain.ExecuteSQL(strSQL);
	}
	catch (CDBException* e)
	{
		CString str;
		str.Format("db:%s %s",e->m_strError,strSQL);
		
//		AfxMessageBox(str);
		e->Delete();
	}
	// 	char name[MAX_PATH] = {0};
// 	sprintf(name,"%s%s.log",GetCurrentPathName(),CTime::GetCurrentTime().Format("%Y%m%d"));
// 	
// 	CLocker lock(m_ctLockFile);
// 	CStdioFile file;
// 	if(file.Open(name,CFile::modeWrite|CFile::modeNoTruncate|CFile::modeCreate))
// 	{
// 		file.SeekToEnd();
// 		file.Write(data,strlen(data));
// 		file.Close();
// 	}

}

void CDataBase::ExcuteSQL(char* pSQL)
{
	try
	{	
		m_dbMain.ExecuteSQL(pSQL);
	}
	catch (CDBException* e)
	{
		CString str;
		str.Format("db:%s %s",e->m_strError,pSQL);
		
//		AfxMessageBox(str);
		e->Delete();
	}
}

int CDataBase::Tcp_Send(_DTU_ *pDTU,SOCKET s,char* pBuff,int nLen,int nSecond)
{
	int n = send(s,pBuff,nLen,0);

	int d;
	if(n > 0)
	{
		m_uSendLen += n;

		if(pDTU)
		{
/*			_RECV__LOG___* pLog = new _RECV__LOG___;
			pLog->nType = 2;
			pLog->time = GetTime();
			strcpy(pLog->ip,pDTU->ip);
			pLog->port = atoi(pDTU->port);
			pLog->nLen = nLen;
			pLog->buff = new BYTE[nLen + 1];
			memcpy(pLog->buff,pBuff,nLen);
			pDTU->m_RecvListt.AddTail(pLog);
			
			if(pDTU->m_RecvListt.GetCount() > m_nLogLine)
			{
				_RECV__LOG___* pLog = (_RECV__LOG___* )pDTU->m_RecvListt.GetHead();
				pDTU->m_RecvListt.RemoveHead();
				delete []pLog->buff;
				delete pLog;
			}*/
		}
	}
	else
	{
		d = GetLastError();
	}

	return n;
/*
	int   status,nbytessended;
	if(s == -1)
	{
		return   0;
	}
	struct   timeval   tv = {nSecond,0};
	fd_set   fd;
	FD_ZERO(&fd);
	FD_SET(s,&fd);
	if(nSecond == 0)
	{
		status=select(s+1,(fd_set   *)NULL,&fd,(fd_set   *)NULL,NULL);
	}
	else
	{
		status=select(s+1,(fd_set   *)NULL,&fd,(fd_set   *)NULL,&tv);
	}
	switch(status)
	{
	case   -1:
		//printf("read   select   error\n");
		return   -1;
	case   0:
		//printf("receive   time   out\n");
		return   0;
	default:
		if(FD_ISSET(s,&fd))
		{
			if((nbytessended = send(s,pBuff,nLen,0)) == -1)
			{
				return   0;
			}
			else
			{
				m_uSendLen += nbytessended;
				return   nbytessended;
			}
		}
	}
	return 0;*/
}

int CDataBase::Tcp_Recv(SOCKET s,char* pBuff,int nLen,int nSecond)
{
	int n = recv(s,pBuff,nLen,0);
	if(n > 0)
	{
		m_uRecvLen += n;
	}
	return n;
/*	int   status,nbytesreceived;
	if(s==-1)
	{
		return   0;
	}
	struct   timeval   tv={nSecond,0};
	fd_set   fd;
	FD_ZERO(&fd);
	FD_SET(s,&fd);
	if(nSecond == 0)
	{
		status=select(s+1,&fd,(fd_set   *)NULL,(fd_set   *)NULL,NULL);
	}
	else
	{
		status=select(s+1,&fd,(fd_set   *)NULL,(fd_set   *)NULL,&tv);
	}
	switch(status)
	{
	case   -1:
		//printf("read   select   error\n");
		return   -1;
	case   0:
		//printf("receive   time   out\n");
		return   0;
	default:
		if(FD_ISSET(s,&fd))
		{
			if((nbytesreceived=recv(s,pBuff,nLen,0)) == -1)
			{
				//	int kkk=0;
				//	kkk=WSAGetLastError();
				return   0;
			}
			else
			{
				m_uRecvLen += nbytesreceived;
				return   nbytesreceived;
			}
		}
	}
	return 0;*/
}

BOOL CDataBase::GetView()
{
	CString strFileName;
	
	strFileName.Format("%sSystem.ini",GetCurrentPathName());
	
	int nView = ReadInt("Config","View",0,strFileName);
	
	if(nView == 1)
		return TRUE;
	return FALSE;
}

void CDataBase::SetView(int nView)
{
	CString strFileName;
	
	strFileName.Format("%sSystem.ini",GetCurrentPathName());
	
	WriteInt("Config","View",nView,strFileName);
}

void CDataBase::onNewConnection(int serverId, int fd) {
	if (serverId == 0) {
		addAClientToList(fd);
	} else {
		addBClientToList(fd);
	}
}

void CDataBase::addBClientToList(int fd) {
	struct sockaddr_in ip_adr_get;
	int ip_adr_len;

	ip_adr_len = sizeof(ip_adr_get);
	getpeername(fd, (sockaddr*)&ip_adr_get, &ip_adr_len);
	_CLIENT_* pClient = new _CLIENT_;
	memset(pClient, 0, sizeof(_CLIENT_));
	pClient->sSocket = fd;
	pClient->dwConnectTime = GetTime();
	strcpy(pClient->ip, inet_ntoa(ip_adr_get.sin_addr));
	sprintf(pClient->port, "%d", ntohs(ip_adr_get.sin_port));
	pClient->dwRecvTime = GetTime();
	pClient->nSocketType = 0;

	char strQQ[MAX_PATH] = { 0 };
	sprintf(strQQ, "%s%s", GetCurrentPathName(), "qqwry.dat");
	CLocation location(strQQ);

	char strLocation[MAX_PATH] = { 0 };
	location.GetLocation(pClient->ip, strLocation);
	memset(pClient->m_strLocation, 0, sizeof(pClient->m_strLocation));
	strcpy(pClient->m_strLocation, strLocation);

	CLocker lock(m_ctLockB);
	m_ClientList.AddTail(pClient);

	OutputDebug("New client connect. %s %s", pClient->ip, pClient->port);
}

void CDataBase::removeBClientFromList(int fd) {
	CLocker lock(m_ctLockB);
	for (POSITION pos = m_ClientList.GetHeadPosition();pos;)
	{
		POSITION t = pos;
		_CLIENT_* pClient = (_CLIENT_ *)m_ClientList.GetNext(pos);

		if (pClient->sSocket <= 0)
			continue;
		if (pClient->sSocket == fd) {
			m_ClientList.RemoveAt(t);
			return;
		}
	}
}

_CLIENT_* CDataBase::getClientFromList(int fd) {
	CLocker lock(m_ctLockB);
	for (POSITION pos = m_ClientList.GetHeadPosition();pos;)
	{
		POSITION t = pos;
		_CLIENT_* pClient = (_CLIENT_ *)m_ClientList.GetNext(pos);

		if (pClient->sSocket == fd)
			return pClient;
	}
	return NULL;
}

void CDataBase::onConnectionClosed(int serverId, int fd) {
	if (serverId == 0) {
		removeAClientFromList(fd);
	} else {
		removeBClientFromList(fd);
	}
}

void CDataBase::addAClientToList(int fd) {
	// do not add
}

void CDataBase::removeAClientFromList(int fd) {

}

void CDataBase::sendDataToServerAConnection(int fd, char* data, size_t len) {
	m_serverA->sendToClientByFd(fd, data, len);
}

void CDataBase::sendDataToServerBConnection(int fd, char* data, size_t len) {
	m_serverB->sendToClientByFd(fd, data, len);
}

void CDataBase::sendDataToServerAConnection(char* data, size_t len) {
	m_serverA->sendToAllClients(data, len);
}

void CDataBase::sendDataToServerBConnection(char* data, size_t len) {
	m_serverB->sendToAllClients(data, len);
}

void CDataBase::sendDataToServerBConnectionBySocketType(int socketType, char* data, size_t len) {
	m_serverB->sendToClientBySocketType(socketType, data, len);
}

void CDataBase::Config()
{
	CDialogConfig dlg;
	dlg.m_nPortA = m_nPortA;
	dlg.m_nPortB = m_nPortB;
	dlg.m_nTimeOut = m_nTimeOut;
	dlg.m_nCheckTime = m_nCheckTime;
	dlg.m_nLogDay = m_nLogDay;
	dlg.m_strName = m_strTitle;
	dlg.m_nLogLine = m_nLogLine;
	char dsn[MAX_PATH] = {0},user[MAX_PATH] = {0},pwd[MAX_PATH] = {0};
	
	CString strInitFile = GetInitFile();
//				CString strSQL = ReadString("Config","ODBC","ODBC;DSN=main;UID=user;PWD=pwd",strInitFile);//配置
	
//				GetInfoFromSQL(strSQL,dsn,user,pwd);
//				dlg.m_strDsn.Format("%s",dsn);
//				dlg.m_strUser.Format("%s",user);
//				CString strNew;
//				m_des.DecryptAnyLength(pwd,TRUE,strNew);
//				dlg.m_strPwd.Format("%s",strNew);
	
	if(dlg.DoModal() == IDOK)
	{
		strcpy(m_strTitle,dlg.m_strName);
		m_nPortA = dlg.m_nPortA;
		m_nPortB = dlg.m_nPortB;
		m_nTimeOut = dlg.m_nTimeOut;
		m_nCheckTime = dlg.m_nCheckTime;
		m_nLogDay = dlg.m_nLogDay;
		m_nLogLine = dlg.m_nLogLine;

		
		CString strInitFile = GetInitFile();
		
		WriteInt("Config","PortA",m_nPortA,strInitFile);
		WriteInt("Config","PortB",m_nPortB,strInitFile);
		WriteInt("Config","TimeOut",m_nTimeOut,strInitFile);
		WriteInt("Config","CheckTime",m_nCheckTime,strInitFile);
		WriteInt("Config","MaxLog",m_nLogDay,strInitFile);
		WriteString("Config","Title",m_strTitle,strInitFile);
		WriteInt("Config","MaxLogLine",m_nLogLine,strInitFile);

		
/*					if(dlg.m_strPwd.IsEmpty())
		{
			dlg.m_strPwd = " ";
		}
		CString str;
		m_des.EncryptAnyLength(dlg.m_strPwd,TRUE,strNew);
		
		CString sssssss;
		m_des.DecryptAnyLength(strNew,TRUE,sssssss);
		
		str.Format("ODBC;DSN=%s;UID=%s;PWD=%s",
			dlg.m_strDsn,dlg.m_strUser,strNew);
		
		WriteString("Config","ODBC",str,strInitFile);*/
	}
}

BOOL CDataBase::LoadInitFile()
{
	CString strInitFile = GetInitFile();
	m_nPortA = ReadInt("Config","PortA",9988,strInitFile);
	WriteInt("Config","PortA",m_nPortA,strInitFile);
	m_nPortB = ReadInt("Config","PortB",9989,strInitFile);
	WriteInt("Config","PortB",m_nPortB,strInitFile);
	
	m_nTimeOut = ReadInt("Config","TimeOut",60,strInitFile);	
	WriteInt("Config","TimeOut",m_nTimeOut,strInitFile);
	
	m_nCheckTime = ReadInt("Config","CheckTime",120,strInitFile);	
	WriteInt("Config","CheckTime",m_nCheckTime,strInitFile);

	strcpy(m_strTitle,ReadString("Config","Title","未定义",strInitFile));
	WriteString("Config","Title",m_strTitle,strInitFile);



	m_nLogDay = ReadInt("Config","MaxLog",7,strInitFile);
	m_nLogLine = ReadInt("Config","MaxLogLine",1000,strInitFile);

	WriteInt("Config","MaxLog",m_nLogDay,strInitFile);
	WriteInt("Config","MaxLogLine",m_nLogLine,strInitFile);


	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		OutputDebug("WSAStartup err.");
		return FALSE;
	}

	
	m_serverThreadPool.emplace_back([](void* data) {
		CDataBase* dataBase = (CDataBase*)data;
		dataBase->startServerA();
	}, this);
	//m_sListenSocketA = socket(AF_INET, SOCK_STREAM, 0);
	OutputDebug("1 Listen .... %d",m_nPortA);
	
	m_serverThreadPool.emplace_back([](void* data) {
		CDataBase* dataBase = (CDataBase*)data;
		dataBase->startServerB();
	}, this);
	//m_sListenSocketB = socket(AF_INET, SOCK_STREAM, 0);
	OutputDebug("2 Listen .... %d",m_nPortB);

	return TRUE;
}

void CDataBase::InsertCommData(int dtu_id,BYTE* pData,int nLen)
{
	char strData[100] = {0};
	char data[10] = {0};
	for(int i = 0;i < nLen;i ++)
	{
		sprintf(data,"%02X ",pData[i]);
		strcat(strData,data);
	}
	CString strTime;
	strTime = CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S");
	char strSQL[1024] = {0};
	sprintf(strSQL,"insert into METER_COMM (DTU_ID,METER_ID,DATA,COMM_TIME) values " \
		"(%d,%d,\'%s\',#%s#)",
		dtu_id,pData[1],strData,strTime
		);
	
	ExcuteSQL(strSQL,NULL);
}

#ifdef connectSql2008
void CDataBase::testmengON(void)
{
	CString strTemp;
	//自己修改字段名即可
	//strTemp.Format("Update dbo.Dev_NetState set DevIP=%d,EmpID='%s' )", deviceID, id);
	//strTemp.Format("Update dbo.Dev_NetState set ConnectOK=%d,EmpID=%d ", id,deviceID);
	//strTemp.Format("Update dbo.Dev_NetState set ConnectOK=%d ", 1);//注意字符串就用'%s',数字用%d
	//_bstr_t sql = strTemp.GetBuffer(0);
	char strSQL[1024] = { 0 };
	sprintf(strSQL, "Update dbo.Dev_NetState set ConnectOK=%d ", 1);

	ExcuteSQL(strSQL, NULL);


}
void CDataBase::testmengOFF(void)
{
	CString strTemp;
	//自己修改字段名即可
	//strTemp.Format("Update dbo.Dev_NetState set DevIP=%d,EmpID='%s' )", deviceID, id);
	//strTemp.Format("Update dbo.Dev_NetState set ConnectOK=%d,EmpID=%d ", id,deviceID);
	//strTemp.Format("Update dbo.Dev_NetState set ConnectOK=%d ", 1);//注意字符串就用'%s',数字用%d
	//_bstr_t sql = strTemp.GetBuffer(0);
	char strSQL[1024] = { 0 };
	sprintf(strSQL, "Update dbo.Dev_NetState set ConnectOK=%d ", 0);

	ExcuteSQL(strSQL, NULL);


}
#endif