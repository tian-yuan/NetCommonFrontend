// DynamicBulkSet.cpp : implementation file
//

#include "stdafx.h"
#include "DynamicBulkSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDynamicBulkSet

IMPLEMENT_DYNAMIC(CDynamicBulkSet, CRecordset)

CDynamicBulkSet::CDynamicBulkSet(CDatabase* pdb)
: CRecordset(pdb)
{
	m_nDefaultType = dynaset;
	m_nFields = 0;
	
	m_ppvData = NULL;
	m_ppvLengths = NULL;
	
	m_dwRowsetSize = 10000;
}

BOOL CDynamicBulkSet::Open(LPCTSTR lpszSQL)
{
	return CRecordset::Open(CRecordset::snapshot, lpszSQL, CRecordset::readOnly | CRecordset::useMultiRowFetch);
}

void CDynamicBulkSet::Close()
{
	CRecordset::Close();
	
	delete [] m_ppvData;
	delete [] m_ppvLengths;
}

void CDynamicBulkSet::DoBulkFieldExchange(CFieldExchange* pFX)
{
	if (pFX->m_nOperation == CFieldExchange::AllocMultiRowBuffer &&
		m_nFields == 0)
	{
		m_nFields = GetODBCFieldCount();

		m_ppvData = new void*[m_nFields];
		memset(m_ppvData, 0, sizeof(void*) * m_nFields);
		m_ppvLengths = new void*[m_nFields];
		memset(m_ppvLengths, 0, sizeof(void*) * m_nFields);
	}
	
	ASSERT(m_nFields != 0);
	
	pFX->SetFieldType(CFieldExchange::outputColumn);
	for (UINT nNum = 0; nNum < m_nFields; nNum++)
	{
		RFX_Text_Bulk(pFX, _T("Dummy"), (LPSTR*)&m_ppvData[nNum],
			(long**)&m_ppvLengths[nNum], MAX_TEXT_LEN);
	}
}
