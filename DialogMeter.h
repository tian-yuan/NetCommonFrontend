#if !defined(AFX_DIALOGMETER_H__4A7F758B_8075_4521_8407_D54F61CD2239__INCLUDED_)
#define AFX_DIALOGMETER_H__4A7F758B_8075_4521_8407_D54F61CD2239__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogMeter.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogMeter dialog
#include "resource.h"

class CDialogMeter : public CDialog
{
// Construction
public:
	CDialogMeter(CWnd* pParent = NULL);   // standard constructor

	void* m_pMeter;
// Dialog Data
	//{{AFX_DATA(CDialogMeter)
	enum { IDD = IDD_DIALOG_METER };
	CEdit	m_edType;
	CListCtrl	m_lstMeter;
	CEdit	m_edTimeSend;
	CEdit	m_edTimeRecv;
	CEdit	m_edData;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogMeter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogMeter)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonRefresh();
	afx_msg void OnButtonType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGMETER_H__4A7F758B_8075_4521_8407_D54F61CD2239__INCLUDED_)
