// DialogView.cpp : implementation file
//

#include "stdafx.h"

#include <WINSVC.H>


#include "DialogView.h"

#include "UserDefine.h"
#include "Locker.h"

#include "DialogText.h"
#include "DialogConfig.h"
#include "DES.h"
#include "DataBase.h"

#include "DialogMeter.h"
#include "DialogDTU.h"

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/
/////////////////////////////////////////////////////////////////////////////
// CDialogView dialog

// extern int g_nPortA;//主控通信端口 
// extern int g_nPortB;//分控通信端口 
// 
// extern int g_nTimeOut;//超时时间

extern CDataBase g_dbMain;

//extern pthread_mutex_t g_ctConnectLock;
//extern WAIT_LIST g_WaitList;
//extern CONNECT_LIST g_ConnectList;
//extern CDES g_des;


LRESULT  CDialogView::OnNcHitTest(CPoint pt)
{
	CRect rc;
	GetClientRect(&rc);
	ClientToScreen(&rc);
	return rc.PtInRect(pt) ? HTCAPTION : CDialog::OnNcHitTest(pt);
}


CDialogView::CDialogView(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogView::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDialogView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogView)
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_edConfig);
	DDX_Control(pDX, IDC_BUTTON_LOAD, m_edLoad);
	DDX_Control(pDX, IDC_BUTTON_REG, m_edReg);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_edDel);
	DDX_Control(pDX, IDC_BUTTON_CHECK, m_edCheck);
	DDX_Control(pDX, IDCANCEL, m_edExit);
	DDX_Control(pDX, IDC_LIST_CONNECT, m_lstConnect);
	DDX_Control(pDX, IDC_LIST_WAIT, m_lstClient);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogView, CDialog)
	//{{AFX_MSG_MAP(CDialogView)
	ON_WM_TIMER()
	ON_COMMAND(ID_TCP_CLOSE, OnTcpClose)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONNECT, OnRclickListConnect)
	ON_COMMAND(ID_TCP_SEND, OnTcpSend)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REG, OnButtonReg)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_CHECK, OnButtonCheck)
	ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_CONNECT, OnGetdispinfoListConnect)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, OnButtonConfig)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONNECT, OnDblclkListConnect)
	ON_COMMAND(ID_CLIENT_REMOVE, OnClientRemove)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_WAIT, OnRclickListWait)
	ON_COMMAND(ID_CLIENT_CLEAR, OnClientClear)
	ON_COMMAND(ID_CLIENT_REFRESH, OnClientRefresh)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnButtonLoad)
	//}}AFX_MSG_MAP
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogView message handlers

BOOL CDialogView::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	DWORD dwNewStyle = LVS_EX_FULLROWSELECT;//|LVS_EX_CHECKBOXES;
	
	m_lstConnect.SetDefaultStyle();
	m_lstConnect.SetExtendedStyle(dwNewStyle);	
	
	m_lstConnect.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_lstConnect.InsertColumn(0,"ID",LVCFMT_LEFT,30);
	m_lstConnect.InsertColumn(1,"DTU_ID",LVCFMT_LEFT,220);
	m_lstConnect.InsertColumn(2,"METER",LVCFMT_RIGHT,50);

	m_lstConnect.InsertColumn(3,"地址",LVCFMT_LEFT,110);
	m_lstConnect.InsertColumn(4,"端口",LVCFMT_RIGHT,60);

	m_lstConnect.InsertColumn(5,"连接时间",LVCFMT_LEFT,130);

	m_lstConnect.InsertColumn(6,"DTU时间",LVCFMT_RIGHT,60);

	m_lstConnect.InsertColumn(7,"接收",LVCFMT_RIGHT,70);
	m_lstConnect.InsertColumn(8,"发送",LVCFMT_RIGHT,70);
	m_lstConnect.InsertColumn(9,"状态",LVCFMT_LEFT,50);


	m_lstClient.SetDefaultStyle();
	m_lstClient.SetExtendedStyle(dwNewStyle);	
	
	m_lstClient.InsertColumn(0,"顺序",LVCFMT_LEFT,50);

	m_lstClient.InsertColumn(1,"地址",LVCFMT_LEFT,120);
	m_lstClient.InsertColumn(2,"端口",LVCFMT_RIGHT,60);

	m_lstClient.InsertColumn(3,"连接时间",LVCFMT_RIGHT,130);
	m_lstClient.InsertColumn(4,"接收时间",LVCFMT_RIGHT,80);
	m_lstClient.InsertColumn(5,"发送时间",LVCFMT_RIGHT,80);
	m_lstClient.InsertColumn(6,"状态",LVCFMT_LEFT,80);
	m_lstClient.InsertColumn(7,"地址",LVCFMT_LEFT,200);
	
	UpdateList1();
	UpdateList2();

	SetTimer(1,5000,NULL);
	SetTimer(1001,1000 * 60 * 3,NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogView::UpdateList1()  //定时器调用，5秒1次
{
	int m_nSel = -1;
	POSITION ppPops = m_lstConnect.GetFirstSelectedItemPosition();
	if(ppPops)
	{
		m_nSel = m_lstConnect.GetNextSelectedItem(ppPops);
	}
	//g_dbMain
	long tt = g_dbMain.GetTime();
	POSITION pos;
	int j = 0;
//	CLocker lock(g_ctConnectLock);
	CString strID,strMeterID,
		strPort,strConnect,
		strDtuRecv,strMeterRecv,strMeterSend,strStatus;

	int dtu_num = 0,meter_num = 0;
	for(pos = g_dbMain.m_DTUList.GetHeadPosition();pos;)
	{
		_DTU_* pDTU = (_DTU_* )g_dbMain.m_DTUList.GetNext(pos);
		dtu_num ++;

		int index = m_lstConnect.GetItemCount();
		if(j >= index)
		{
			CItemData info;
			m_lstConnect.InsertData(info);
		}
		strID.Format("%d",j + 1);
		strConnect.Format("%s",g_dbMain.GetTimeString(pDTU->dwConnectTime,TRUE));

		strDtuRecv.Format("%s",g_dbMain.GetTimeString(pDTU->dwRecvActiveTime,FALSE));
		
		if(pDTU->sSocket > 0)
		{
			strStatus.Format("连接");				
		}
		else if(pDTU->sSocket == 0)
		{
			strStatus.Format("断开");
		}
		else
		{
			strStatus.Format("未连接");
		}
		m_lstConnect.SetItemText(j,0,strID);
		m_lstConnect.SetItemText(j,1,pDTU->str_ID);
		m_lstConnect.SetItemText(j,2,"");
		
		m_lstConnect.SetItemText(j,3,pDTU->ip);
		m_lstConnect.SetItemText(j,4,pDTU->port);			
		m_lstConnect.SetItemText(j,5,strConnect);
		
		m_lstConnect.SetItemText(j,6,strDtuRecv);
		
		m_lstConnect.SetItemText(j,7,"");
		m_lstConnect.SetItemText(j,8,"");
		m_lstConnect.SetItemText(j,9,strStatus);

		j ++;
		for(POSITION posT = pDTU->m_MeterList.GetHeadPosition();posT;j ++)
		{
			_METER_* pMeter = (_METER_ *)pDTU->m_MeterList.GetNext(posT);

			meter_num ++;
		
			int index = m_lstConnect.GetItemCount();
			if(j >= index)
			{
				CItemData info;
				m_lstConnect.InsertData(info);
			}

			strID.Format("%d",j + 1);
			strMeterID.Format("%d",pMeter->nID);
			
			strMeterRecv.Format("%s",g_dbMain.GetTimeString(pMeter->dwRecvMeterDataTime,FALSE));
			strMeterSend.Format("%s",g_dbMain.GetTimeString(pMeter->dwSendMeterDataTime,FALSE));

			
			m_lstConnect.SetItemText(j,0,strID);
			m_lstConnect.SetItemText(j,1,"");
			m_lstConnect.SetItemText(j,2,strMeterID);
			
			m_lstConnect.SetItemText(j,3,"");
			m_lstConnect.SetItemText(j,4,"");			
			m_lstConnect.SetItemText(j,5,"");
			
			m_lstConnect.SetItemText(j,6,"");
			
			m_lstConnect.SetItemText(j,7,strMeterRecv);
			m_lstConnect.SetItemText(j,8,strMeterSend);

			DWORD d = g_dbMain.GetTime();
			if(abs((long)(d - pMeter->dwRecvMeterDataTime)) <= g_dbMain.m_nTimeOut * 2)
			{
				strStatus.Format("∨");
			}
			else
			{
				strStatus.Format("×");
			}
			m_lstConnect.SetItemText(j,9,strStatus);

		}
	}
	int n = j;
	int m = m_lstConnect.GetItemCount();
	for(;j < m;j ++)
	{
		m_lstConnect.DeleteItem(n);
	}
	m_lstConnect.SetCount();
	if(m_nSel != -1)
	{
		if(m_nSel < m_lstConnect.GetItemCount())
		{
			m_lstConnect.EnsureVisible(m_nSel,FALSE);
		}
		else
		{
			m_lstConnect.EnsureVisible(m_lstConnect.GetItemCount() - 1,FALSE);
		}
	}

	int fS = 0;
	if((CTime::GetCurrentTime().GetTime() - g_dbMain.m_lStartTime) != 0)
		fS = g_dbMain.m_uSendLen / (CTime::GetCurrentTime().GetTime() - g_dbMain.m_lStartTime);
	int fR = 0;
	if((CTime::GetCurrentTime().GetTime() - g_dbMain.m_lStartTime) != 0)
		fR = g_dbMain.m_uRecvLen / (CTime::GetCurrentTime().GetTime() - g_dbMain.m_lStartTime);
	CString strTime;
	CTime tStart(g_dbMain.m_lStartTime);
	strTime.Format("%s %d,%d DTU:%d Meter:%d 时间 %s (收到 %d [%dB/S] 发送 %d [%dB/S])",
		g_dbMain.m_strTitle,g_dbMain.m_nPortA,g_dbMain.m_nPortB,
		dtu_num,meter_num,
		CTime::GetCurrentTime().Format("%H:%M:%S"),
		g_dbMain.m_uRecvLen,fR,g_dbMain.m_uSendLen,fS);
	SetWindowText(strTime);
}


void CDialogView::UpdateList2()
{
	int m_nSel = -1;
	POSITION ppPops = m_lstClient.GetFirstSelectedItemPosition();
	if(ppPops)
	{
		m_nSel = m_lstClient.GetNextSelectedItem(ppPops);
	}

	CString strID,strSocket,
		strIP,
		strPort,strConnect,
		strRecvTime,strSendTime,strStatus;
	long tt = g_dbMain.GetTime();
	POSITION pos;
	int j = 0;
//	CLocker lock(m_ctConnectLock);
	for(pos = g_dbMain.m_ClientList.GetHeadPosition();pos;j ++)
	{
		POSITION posT = pos;
		_CLIENT_* pClient = (_CLIENT_* )g_dbMain.m_ClientList.GetNext(pos);
		
		int index = m_lstClient.GetItemCount();
		if(j >= index)
		{
			CItemData info;
			m_lstClient.InsertData(info);
		}
		
		
		strSocket.Format("%d",j + 1);
		strConnect.Format("%s",g_dbMain.GetTimeString(pClient->dwConnectTime,TRUE));

		strRecvTime.Format("%s",g_dbMain.GetTimeString(pClient->dwRecvTime,FALSE));
		strSendTime.Format("%s",g_dbMain.GetTimeString(pClient->dwSendTime,FALSE));
		
		if(pClient->sSocket > 0)
		{
			strStatus.Format("连接");
			if(pClient->nSocketType == 1)
			{
				if(tt - pClient->dwRecvTime > g_dbMain.m_nTimeOut * 2)
				{
					closesocket(pClient->sSocket);
					pClient->sSocket = 0;
					g_dbMain.m_ClientList.RemoveAt(posT);
					delete pClient;
					
					break;
				}
			}
			if(pClient->nSocketType == 0)
			{
				if(tt - pClient->dwRecvTime > g_dbMain.m_nTimeOut)
				{
					closesocket(pClient->sSocket);
					pClient->sSocket = 0;

					g_dbMain.m_ClientList.RemoveAt(posT);
					delete pClient;

				}
			}
			if(pClient->nSocketType == 2)
			{
				if(tt - pClient->dwRecvTime > 60 * 5)
				{
					closesocket(pClient->sSocket);
					pClient->sSocket = 0;

					g_dbMain.m_ClientList.RemoveAt(posT);
					delete pClient;

				}
			}
		}
		else
		{
			strStatus.Format("断开");

			g_dbMain.m_ClientList.RemoveAt(posT);
			delete pClient;


		}
// 		int off = tt - info.dwConnectTime;
// 		if(info.dwConnectTime != 0)
// 			strTime.Format("%02d:%02d:%02d",off / 3600,((off % 3600) / 60),off % 60);
// 		else
// 			strTime = "";
		
		m_lstClient.SetItemText(j,0,strSocket);
		m_lstClient.SetItemText(j,1,pClient->ip);
		m_lstClient.SetItemText(j,2,pClient->port);
		m_lstClient.SetItemText(j,3,strConnect);
	
		m_lstClient.SetItemText(j,4,strRecvTime);
		m_lstClient.SetItemText(j,5,strSendTime);
		m_lstClient.SetItemText(j,6,strStatus);
		m_lstClient.SetItemText(j,7,pClient->m_strLocation);
	}
	int n = j;
	int m = m_lstClient.GetItemCount();
	for(;j < m;j ++)
	{
		m_lstClient.DeleteItem(n);
	}
	m_lstClient.SetCount();

	if(m_nSel != -1)
	{
		if(m_nSel < m_lstClient.GetItemCount())
		{
			m_lstClient.EnsureVisible(m_nSel,FALSE);
		}
		else
		{
			m_lstClient.EnsureVisible(m_lstClient.GetItemCount() - 1,FALSE);
		}
	}

}

void CDialogView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnTimer(nIDEvent);


	if(nIDEvent == 1001)
	{
		;OnButtonLoad();
	}
	else
	{
		UpdateList1();
		UpdateList2();
	}
}

void CDialogView::OnTcpClose() 
{
	// TODO: Add your command handler code here
	POSITION pos = m_lstConnect.GetFirstSelectedItemPosition();
	if(pos)
	{
		int n = m_lstConnect.GetNextSelectedItem(pos);
		if(n >= 0)
		{
			CString ip = m_lstConnect.GetItemText(n,1);
			CString port = m_lstConnect.GetItemText(n,2);

			for(pos = g_dbMain.m_ClientList.GetHeadPosition();pos;)
			{
				POSITION posTT = pos;
				_CLIENT_* pClient = (_CLIENT_* )g_dbMain.m_ClientList.GetNext(pos);
				closesocket(pClient->sSocket);
				delete pClient;
				g_dbMain.m_ClientList.RemoveAt(posTT);
			}
		}
	}
}

void CDialogView::OnRclickListConnect(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_lstConnect.GetFirstSelectedItemPosition();
	if(pos)
	{
		CMenu menu,*pPop = NULL;
		menu.LoadMenu(IDR_MENU_POP);
		
		pPop = menu.GetSubMenu(0);
		
		POINT pt;
		GetCursorPos(&pt);
		
		pPop->TrackPopupMenu(TPM_LEFTALIGN |TPM_RIGHTBUTTON, pt.x, 
			pt.y, this);
	}
	
	*pResult = 0;
}

void CDialogView::OnTcpSend() 
{
	// TODO: Add your command handler code here
	POSITION pos = m_lstConnect.GetFirstSelectedItemPosition();
	if(pos)
	{
		int n = m_lstConnect.GetNextSelectedItem(pos);
		if(n >= 0)
		{
			CString strDTU = m_lstConnect.GetItemText(n,1);
			if(!strDTU.IsEmpty())//DTU item
			{
				char strD[128] = {0};
				strcpy(strD,strDTU);
				_DTU_* pDTU = g_dbMain.FindDTU(strD);
				g_dbMain.QueryDevice(pDTU);
			}
			else//meter item
			{
				CString strID = m_lstConnect.GetItemText(n,2);

				_METER_* pMeter = g_dbMain.FindMeter(atoi(strID));
				g_dbMain.QueryDevice(pMeter);
			}
		}
	}
}

void CDialogView::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if(m_lstConnect.GetSafeHwnd())
	{
		int h = (cy - 10) / 2;
		m_lstConnect.MoveWindow(5,5,cx - 10,cy -h);
		m_lstClient.MoveWindow(5,cy - h + 15,cx - 10,cy - h - 70);

		
		m_edExit.MoveWindow(cx - 90,cy - 5 - 25,70,20);



		m_edConfig.MoveWindow(30,cy - 5 - 25,70,20);

		m_edCheck.MoveWindow(120,cy - 5 - 25,70,20);

		m_edReg.MoveWindow(210,cy - 5 - 25,70,20);

		m_edDel.MoveWindow(300,cy - 5 - 25,70,20);


		m_edLoad.MoveWindow(390,cy - 5 - 25,70,20);
	}
}

void CDialogView::OnButtonReg() 
{
	// TODO: Add your control notification handler code here
/*	char strPath[MAX_PATH] = {0};

	sprintf(strPath,"create _MainServer binpath= \"%s\"",g_dbMain.GetThisFileName());
	
	ShellExecute(NULL, "open", "sc", strPath, NULL, SW_HIDE);
	
	ShellExecute(NULL, "open", "sc", "config _MainServer start= auto", NULL, SW_HIDE);
	
	ShellExecute(NULL, "open", "sc", "description _MainServer \"_MainServer --- comm with DTU.\"", NULL, SW_HIDE);

	ShellExecute(NULL, "open", "sc", "config _MainServer start= auto", NULL, SW_HIDE);

	ShellExecute(NULL, "open", "sc", "start  _MainServer", NULL, SW_HIDE);

	AfxMessageBox("服务安装");*/
}

void CDialogView::OnButtonDel() 
{
	// TODO: Add your control notification handler code here
/*	ShellExecute(NULL, "open", "sc", "config _MainServer start= auto", NULL, SW_HIDE);
	ShellExecute(NULL, "open", "sc", "stop _MainServer", NULL, SW_HIDE);

	Sleep(2000);

	ShellExecute(NULL, "open", "sc", "delete _MainServer", NULL, SW_HIDE);
	AfxMessageBox("服务卸载");*/
}

void CDialogView::OnButtonCheck() 
{
	// TODO: Add your control notification handler code here
	/*
	SC_HANDLE schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(schSCManager == NULL)
	{
		OutputDebug("open service error....");
		AfxMessageBox("打开服务出错");
		return;
	}

	SC_HANDLE schService = OpenService(schSCManager, "_MainServer", SC_MANAGER_ALL_ACCESS);
	if(schService == NULL)
	{
		CloseServiceHandle(schSCManager);
		OutputDebug("open service error");
		AfxMessageBox("打开服务出错");
		return;
	}

	SERVICE_STATUS ServiceStatus;
	
	ServiceStatus.dwWin32ExitCode = 0; 
	ServiceStatus.dwCurrentState = 0; 

	QueryServiceStatus(schService,&ServiceStatus);
	
	CString strText("unknown");
	switch(ServiceStatus.dwCurrentState)
	{
	case SERVICE_STOPPED:
		strText.Format("服务状态 SERVICE_STOPPED");
		break;
	case SERVICE_START_PENDING:
		strText.Format("服务状态 SERVICE_START_PENDING");
		break;
	case SERVICE_STOP_PENDING:
		strText.Format("服务状态 SERVICE_STOP_PENDING");
		break;
	case SERVICE_RUNNING:
		strText.Format("服务状态 SERVICE_RUNNING");
		break;
	case SERVICE_CONTINUE_PENDING:
		strText.Format("服务状态 SERVICE_CONTINUE_PENDING");
		break;
	case SERVICE_PAUSE_PENDING:
		strText.Format("服务状态 SERVICE_PAUSE_PENDING");
		break;
	case SERVICE_PAUSED:
		strText.Format("服务状态 SERVICE_PAUSED");
		break;
	default:
		break;
	}
	CloseServiceHandle(schSCManager);

	AfxMessageBox(strText);*/
}

void CDialogView::OnGetdispinfoListConnect(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}
void Config();

void CDialogView::OnButtonConfig() 
{
	// TODO: Add your control notification handler code here
	Config();

}

void CDialogView::OnDblclkListConnect(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_lstConnect.GetFirstSelectedItemPosition();
	if(pos)
	{
		int n = m_lstConnect.GetNextSelectedItem(pos);
		CString DTU = m_lstConnect.GetItemText(n,1);
		if(DTU.IsEmpty())
		{
			CString ID = m_lstConnect.GetItemText(n,2);

			_METER_* pMeter = g_dbMain.FindMeter(atoi(ID));

			if(pMeter)
			{
				CDialogMeter dlg;
				dlg.m_pMeter = pMeter;
				dlg.DoModal();
			}
		}
		else
		{
			char ID[100] = {0};
			strcpy(ID,DTU);
			_DTU_* pDTU = g_dbMain.FindDTU(ID);
			if(pDTU)
			{
				CDialogDTU dlg;
				dlg.m_pDTU = pDTU;
				dlg.DoModal();
			}
		}

	}
	*pResult = 0;
}


void CDialogView::OnRclickListWait(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_lstClient.GetFirstSelectedItemPosition();
	if(pos)
	{
		CMenu menu,*pPop = NULL;
		menu.LoadMenu(IDR_MENU_POP);
		
		pPop = menu.GetSubMenu(1);
		
		POINT pt;
		GetCursorPos(&pt);
		
		pPop->TrackPopupMenu(TPM_LEFTALIGN |TPM_RIGHTBUTTON, pt.x, 
			pt.y, this);
	}
	
	*pResult = 0;
}

void CDialogView::OnClientRemove() 
{
	// TODO: Add your command handler code here
	POSITION pos = m_lstClient.GetFirstSelectedItemPosition();
	if(pos)
	{
		int n = m_lstClient.GetNextSelectedItem(pos);
		m_lstClient.DeleteItem(n);
	}
}

void CDialogView::OnClientClear() 
{
	// TODO: Add your command handler code here
	for(POSITION pos = g_dbMain.m_ClientList.GetHeadPosition();pos;)
	{
		POSITION posT = pos;
		_CLIENT_* pClient = (_CLIENT_* )g_dbMain.m_ClientList.GetNext(pos);
		if(pClient->sSocket <= 0)
		{
			g_dbMain.m_ClientList.RemoveAt(posT);
			delete pClient;
		}
	}
	UpdateList2();

}

void CDialogView::OnClientRefresh() 
{
	// TODO: Add your command handler code here
	UpdateList2();
}

void CDialogView::OnButtonLoad() 
{
	// TODO: Add your control notification handler code here
//	g_dbMain.LoadBaseTables2();

//	UpdateList1();
}


void CDialogView::OnCancel() 
{
	// TODO: Add extra cleanup here
	if(AfxMessageBox("是否退出系统? ",MB_YESNO) == IDYES)
	{
		CDialog::OnCancel();

	}
}
