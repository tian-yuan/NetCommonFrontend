// DataBase.h: interface for the CDataBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATABASE_H__D242237B_AFF0_4D38_9CE8_44A6B622FA6F__INCLUDED_)
#define AFX_DATABASE_H__D242237B_AFF0_4D38_9CE8_44A6B622FA6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "UserDefine.h"
#include "mydefine.h"
#include "TcpServer\MyConnectionA.h"
#include "TcpServer\MyConnectionB.h"
#include "TcpServer\ConnectManager.h"

struct _RECV__LOG___
{
	int nType;//1接收 2 发送 3信息
	long time;
	char ip[32];
	int port;
	BYTE *buff;
	int nLen;
};

struct _DATA_WAIT//主控的连接 未验证ID
{
	SOCKET s;

	DWORD dwConnectTime;
	char ip[32];
	char port[20];

};

struct _DATA_METER_1//1.0版本 2个字节是数 1个字节是报警
{	
	int nDTU_ID;//DTU编号

	int nID;//ID 设备编号

	int nType1;//类型 == 1

	int nUedE;//已用电量 度 2位小数
	BYTE nUedE_E;//已用电量 报警： 1报警 0不报警
	
	int nLeftE;//剩余电量 度 2位小数
	BYTE nLeftE_E;//剩余电量 报警： 1报警 0不报警
	
	int nUsedM;//已用钱 元 2位小数
	BYTE nUsedM_E;//已用钱 报警： 1报警 0不报警
	
	int nLeftM;//剩余钱 元
	BYTE nLeftM_E;//剩余钱 报警： 1报警 0不报警
	
	int nRate;//单价 元/度 2位小数
	BYTE nRate_E;//单价 报警： 1报警 0不报警
	
	int nFlow;//流量 立方米 2位小数
	BYTE nFlow_E;//流量 报警： 1报警 0不报警
	
	int nFlow2;//瞬间流量 立方米/小时 2位小数
	BYTE nFlow2_E;//瞬间流量  报警： 1报警 0不报警
	
	int nUID;//当前用户ID
	BYTE nUID_E;//当前用户ID 报警： 1报警 0不报警
	
	long lTime;//最后上传的时间 从19
};

struct _DATA_METER_2//2.0版本 3个字节是数 没有报警
{	
	int nDTU_ID2;//DTU编号

	int nID2;//ID 设备编号

	int nType2;//类型 == 2

	int nUedE2;//已用电量 度 2位小数
	BYTE nUedE_E2;// == 0
	
	int nLeftE2;//剩余电量 度 2位小数
	BYTE nLeftE_E2;// == 0
	
	int nUsedM2;//已用钱 元 2位小数
	BYTE nUsedM_E2;// == 0
	
	int nLeftM2;//剩余钱 元
	BYTE nLeftM_E2;// == 0
	
	int nRate2;//单价 元/度 2位小数
	BYTE nRate_E2;// == 0
	
	int nFlowTotal2;//累积流量 立方米
	BYTE nFlowTotal_E2;// == 0
	
	int nFlow22;//瞬间流量 立方米/小时 2位小数
	BYTE nFlow2_E2;// == 0
	
	int nUID2;//当前用户ID
	BYTE nUID_E2;// == 0
	
	long lTime;//最后上传的时间 从19
};

struct _DATA_RTU//3.0
{
	int nDTU_ID3;//DTU编号
	int nID3;//ID 设备编号
	
	int nType3;//类型 == 3

	int nFlow;//流量 立方米 2位小数
	BYTE nFlow_E;// == 0
	
	int nWater;//水位
	BYTE nWater_E;// == 0
	
	int nTempr;//温度  2位小数
	BYTE nTempr_E;//==0
	
	int nHumidity;//湿度
	BYTE nHumidity_E;//==0
	
	int nOther1;//其他1
	BYTE nOther1_E;//==0
	
	int nOther2;//其他2
	BYTE nOther2_E;//==0
	
	int nOther3;//其他3
	BYTE nOther3_E;//==0
	
	int nOther4;//其他4
	BYTE nOther4_E;//==0
	
	long lTime;//最后上传的时间 从19
};

//#define MAX_LOG 1000

struct _DTU_
{
	char str_ID[40];//数据库存放
	SOCKET sSocket;
	
	char ip[32];
	char port[20];

	DWORD dwConnectTime;//连接时间

	DWORD dwRecvActiveTime;//接收心跳时间

	DWORD dwSendDataTime;//发送数据时间

	CPtrList m_MeterList;//_METER_
	BYTE m_strLastRecv[1024];
	int m_nLastLen;


	CPtrList m_RecvListt;//_RECV__LOG___
};

struct _METER_//表
{
	_DATA_METER_1	data1;
	_DATA_METER_2	data2;
	_DATA_RTU		data3;

	int nDTU_ID;
	int nID;//ID 设备编号  数据库存放
	int m_nType;//1 水表1 2  3 RTU

	DWORD dwRecvMeterDataTime;//接收数据时间
	DWORD dwSendMeterDataTime;//发送数据时间

	BYTE recv_data[32];

	_DTU_* pDTU;
};

struct _CLIENT_//pc客户端连接
{
	SOCKET sSocket;

	char ip[32];
	char port[20];

	DWORD dwConnectTime;//连接时间
	DWORD dwSendTime;//发送命令时间
	DWORD dwRecvTime;//接收心跳时间

	int nSocketType;//0 么有收到任何消息时 1 客户端通信的 2 下载数据库的

	char m_strLocation[MAX_PATH];
};

//http://lbsyun.baidu.com/apiconsole/key#

class CDataBase  //AA&&&&
{
public:
	CDataBase();
	virtual ~CDataBase();

public:
	virtual void onNewConnection(int serverId, int fd);
	virtual void onConnectionClosed(int serverId, int fd);

public:
	void Release();

	_DTU_* FindDTU(char* pDTU_ID);
#ifdef addEncryptionCode
	_DTU_* FindDTU2(char* pDTU_ID);
#endif
	_METER_* FindMeter(_DTU_* pDTU,int nID);
	_METER_* FindMeter(int nID);


	BOOL AddODBC();
	BOOL InsertRecord(char *pData);
	BOOL ConnectDB();
	int ExcuteSQL(CString pSQL,_CLIENT_* pClient);
	void ExcuteSQL(char* pSQL);

	void QueryDevice(_DTU_* pDTU);//查询DTU所有的表
	void QueryDevice(_METER_* pMeter);//查询单个表

	BOOL RegisterDBSource(CString strDSName, CString strDBPath,CString strSystemPath);
	CString GetCurrentPathName();

	pthread_mutex_t m_ctLockFile;//日志锁 

	pthread_mutex_t m_ctLockA;
	CPtrList m_DTUList;//_DTU_

	pthread_mutex_t m_ctLockB;
	CPtrList m_ClientList;//_CLIENT_

	pthread_mutex_t m_ctLockW;
	CPtrList m_WaitList;//_DATA_WAIT

	CDatabase m_dbMain;
	CString m_strDSN;
	void LoadBaseTables();//加载数据库中数据
	void LoadBaseTables2();
	BOOL CheckTable(char* pTableName);//检查数据库中是否存在
	BOOL CreateTable(char* pSQL);//创建表

	BOOL CheckIDfromDatabase(char* pDTU_ID,int nMeterID);//看数据库中是否存在


// 	void InsertRecord1(_DATA_METER_1* pData);
// 	void InsertRecord2(_DATA_METER_2* pData);
// 	void InsertRecord3(_DATA_RTU* pData);
	void SendToAllClient(BYTE* buff,int nLen,_DTU_* pDTU,BOOL bSendData = TRUE);//接收主控数据 发送到所有的客户端
	BOOL SendAllMeterToClient(_CLIENT_* pClient);//将所有的表数据发送给客户端
	BOOL SendDtuToAllClient(_DTU_* pDTU);//将所有的DTU数据发送给客户端


	WORD Caluation_CRC16(BYTE Buff[], int nSize);

	CString ReadString(LPCTSTR appname, LPCTSTR keyname,LPCTSTR default_value, LPCTSTR file_name);
	
	int ReadInt(LPCTSTR appname, LPCTSTR keyname,int default_value, LPCTSTR file_name);
	
	void WriteString(LPCTSTR appname, LPCTSTR keyname, LPCTSTR text, LPCTSTR file_name);
	
	void WriteInt(LPCTSTR appname, LPCTSTR keyname, int value, LPCTSTR file_name);
	
	CString GetInitFile();
	
	CString GetTimeString(DWORD dwTime = 0,BOOL bAll = FALSE);
	
	long GetTime();
	CString GetThisFileName();

	void WriteLog(int nType,const char * fmt, ...);


	BOOL ZipFile(CString strFile);//压缩文件 压缩完后文件名+.zip
	BOOL UnZipFile(CString strFile);//解压文件 加压完后文件名-.zip

	UINT m_uRecvLen;
	UINT m_uSendLen;

	long m_lStartTime;

	int Tcp_Send(_DTU_ *pDTU,SOCKET s,char* pBuff,int nLen,int nSecond = 1);
	
	int Tcp_Recv(SOCKET s,char* pBuff,int nLen,int nSecond = 1);

	BOOL GetView();
	
	void SetView(int nView);

	void Config();
	BOOL LoadInitFile();
//	void ReLoaddb();//重新加载
	void InsertCommData(int dtu_id,BYTE* pData,int nLen);
#ifdef connectSql2008
	void testmengON(void);
	void testmengOFF(void);
#endif

public: 
	_CLIENT_* getClientFromList(int fd);
	_METER_* getMeterFromList(int nMeterID);
	_DTU_* getDtuFromList(int fd);

public:
	void startServerA() {
		std::string addr;
		addr += "0.0.0.0:";
		addr += std::to_string(m_nPortA);
		m_serverA->setNewConnectionCallback([this](int fd) {
			this->onNewConnection(0, fd);
		});
		m_serverA->setConnectionCloseCallback([this](int fd) {
			this->onConnectionClosed(0, fd);
		});
		m_serverA->start(addr);///&&&&&
	}
	void startServerB() {
		std::string addr;
		addr += "0.0.0.0:";
		addr += std::to_string(m_nPortB);
		m_serverB->setNewConnectionCallback([this](int fd) {
			this->onNewConnection(1, fd);
		});
		m_serverB->setConnectionCloseCallback([this](int fd) {
			this->onConnectionClosed(1, fd);
		});
		m_serverB->start(addr);
	}
	void addBClientToList(int fd);
	void removeBClientFromList(int fd);

	void addAClientToList(int fd);
	void removeAClientFromList(int fd);
	void sendDataToServerAConnection(int fd, char* data, size_t len);
	void sendDataToServerBConnection(int fd, char* data, size_t len);
	void sendDataToServerAConnection(char* data, size_t len);
	void sendDataToServerBConnection(char* data, size_t len);
	void sendDataToServerBConnectionBySocketType(int socketType, char* data, size_t len);

public:

	SOCKET m_sListenSocketA;// = 0;//主控监听
	SOCKET m_sListenSocketB;// = 0;//分控监听
	
	int m_nPortA;// = 9988;//主控端口 +1 分控端口
	int m_nPortB;// = m_nPortA + 1;//分控端口
	
	int m_nTimeOut;// = 60;//超时时间
	
	int m_nCheckTime;// = 120;//查询时间
	
	int m_nLogDay;// = 7;//最大日志天数

	int m_nLogLine;

	char m_strTitle[MAX_PATH];
	std::shared_ptr<ConnectManager<MyConnectionA>> m_serverA;
	std::shared_ptr<ConnectManager<MyConnectionB>> m_serverB;
	std::vector<std::thread> m_serverThreadPool;
};

#endif // !defined(AFX_DATABASE_H__D242237B_AFF0_4D38_9CE8_44A6B622FA6F__INCLUDED_)
