#if !defined(AFX_DIALOGDTU_H__9AE5F939_EF5A_4765_A105_8388F4BEEFB9__INCLUDED_)
#define AFX_DIALOGDTU_H__9AE5F939_EF5A_4765_A105_8388F4BEEFB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogDTU.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogDTU dialog
#include "resource.h"

class CDialogDTU : public CDialog
{
// Construction
public:
	CDialogDTU(CWnd* pParent = NULL);   // standard constructor

	void* m_pDTU;
// Dialog Data
	//{{AFX_DATA(CDialogDTU)
	enum { IDD = IDD_DIALOG_DTU };
	CEdit	m_edNum;
	CListCtrl	m_lstMeter;
	CEdit	m_edActiveTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogDTU)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogDTU)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonRefresh();
	afx_msg void OnButtonComm();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGDTU_H__9AE5F939_EF5A_4765_A105_8388F4BEEFB9__INCLUDED_)
