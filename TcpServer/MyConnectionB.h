#pragma once

#include "ConnectBase.h"

class MyConnectionB : public ConnectBase {
public:
	MyConnectionB(evpp::TCPConnPtr tcpConn) : ConnectBase(tcpConn) {}

	void onMsg(evpp::Buffer* msg) {
		if (msg == nullptr || msg->length() <= 0) {
			LOG_ERROR << "msg is empty.";
			return;
		}
		size_t dataLen = msg->length();
		LOG_INFO << "receive data len : " << dataLen;
		onRead(msg->data(), dataLen);
		msg->NextAll();
	}

private:
	void onRead(const char* data, const size_t& numBytes);
	void onQueryTime(const char* data, const size_t& numBytes);
	void onDownloadFile(const char* data, const size_t& numBytes);
	void onConfigUpdate(const char* data, const size_t& numBytes);
	void onDeviceDataToDtu(const char* data, const size_t& numBytes);
	void onHeartBeat(const char* data, const size_t& numBytes);
};