#pragma once

#include "ConnectBase.h"

class MyConnectionA : public ConnectBase {
public:
	MyConnectionA(evpp::TCPConnPtr tcpConn) : ConnectBase(tcpConn) {}

	void onMsg(evpp::Buffer* msg) {
		if (msg == nullptr || msg->length() <= 0) {
			return;
		}
		size_t dataLen = msg->length();
		LOG_INFO << "receive data len : " << dataLen;
		onRead(msg->data(), dataLen);
		msg->NextAll();
	}
	
private:
	void onRead(const char* data, const size_t& numBytes);
	void onHeartBeat(const char* data, const size_t& numBytes);
	void onRegister(const char* data, const size_t& numBytes);
	void onDataToDevices(const char* data, const size_t& numBytes);
};