#include "StdAfx.h"

#include <thread>
#define SERVICE_IMPLEMENT
#ifdef SERVICE_IMPLEMENT
#include "../DataBase.h"
#include "../Util/log.h"
extern CDataBase g_dbMain;
#endif

#include "MyConnectionB.h"

extern int g_nVer;
extern unsigned long long_to_4bcd(unsigned long value);

void MyConnectionB::onRead(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	std::thread::id this_id = std::this_thread::get_id();
	OutputDebug("current thread : %d", this_id);
	OutputDebug("8899current thread : %d", numBytes);
	std::string sdata(data, numBytes);
	OutputDebug("8899receive data : %s", sdata.c_str());
	/*
	
	std::string sdata(data, numBytes);
	OutputDebug("receive data : %s", sdata.c_str());
	const char* msg = "hoi\n";
	send(msg, strlen(msg));
	*/
	if (numBytes <= 0) {
		// remove client from list
		return;
	}

	unsigned char Ndata[1024];		int nType = 0;
	unsigned char NnumBytes; int	pm ;
	unsigned char Now_numBytes = 0; unsigned   char * pTempm;  char pDATAM[1024];

	if (numBytes < 4) return;
	
	NnumBytes = numBytes;  //Ϊ�˿����룬�滻һ��
	Now_numBytes = numBytes; 
	memcpy(Ndata, data, numBytes);  //Ϊ�˿����룬�滻һ��
	
	pm = 0;
	pTempm = &Ndata[pm];
	memcpy(pDATAM, Ndata, 4); 


nextPACKET:
	//������Ҫ���������ʱ���ղ���������������������?
	//if (tt - pClient->dwRecvTime > g_dbMain.m_nTimeOut * 2)
	//{
	//	g_dbMain.m_ClientList.RemoveAt(t);
	//	closesocket(pClient->sSocket);
	//	pClient->sSocket = 0;
	//	delete pClient;
	//	OutputDebug("��ʱ�Ͽ�.");
	//	continue;
	//}

	if (pDATAM[0] == 0x09) {	//����䵱���������Ĺ��ܣ���Ҫ���ӳ�ʱ�Ͽ�����?
		 if (Now_numBytes==4)
		 {
			 return  onQueryTime(pDATAM, Now_numBytes);
		 }
		 else if (Now_numBytes > 4)
		 {
			 onQueryTime(pDATAM, Now_numBytes);

			 Now_numBytes -= 4; pm +=4;
			 memcpy(pDATAM, &pTempm[pm], 4);
			 goto nextPACKET;
		 }
	}
	

		memcpy(&nType, &pDATAM[0], 4);

		//if (nType == 100 && Now_numBytes == 8)//download
		//{
		//	onDownloadFile(pDATAM, Now_numBytes);
		//}
		//else if (nType == 101)//db Excute  101�Ƕ����������ϴ�����
		//{
		//	onConfigUpdate(pDATAM, Now_numBytes);
		//}
		 if (nType == 102)//db Excute  102�ǵõ���λ�������ݣ��������ضˣ�DTU)
		{
			 memcpy(pDATAM, &pTempm[pm], 45);

			if (Now_numBytes == 45)//45���ֽ�Ϊһ��Э�����?
			{
				onDeviceDataToDtu(pDATAM, 45);
			}
			else if (Now_numBytes > 45)
			{
				onDeviceDataToDtu(pDATAM, 45);

				Now_numBytes -= 45; pm += 45;
				memcpy(pDATAM, &pTempm[pm], 4);
				goto nextPACKET;
			}

   
		}
		else if (data[0] == 0x68 && Now_numBytes == 9) //���������Ӧ�ò���Ҫ�ˣ���һ�����������������
		{
			onHeartBeat(data, Now_numBytes);
		}
#endif
}


void MyConnectionB::onQueryTime(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	_CLIENT_ * pClient = g_dbMain.getClientFromList(m_fd);
	if (pClient == NULL) {
		OutputDebug("on query time for fd : %d, but client is not found.", m_fd);
		return;
	}
	pClient->nSocketType = 1;
	pClient->dwSendTime = g_dbMain.GetTime();
	char buff[30] = { 0 };
	int nLen = 4;
	int nType = 0x09;
	long t = CTime::GetCurrentTime().GetTime();
	memcpy(&buff[0], &nType, 4);
	memcpy(&buff[4], &nLen, 4);
	memcpy(&buff[8], &t, 4);
	send(buff, 12);
#endif
}

void MyConnectionB::onDownloadFile(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	_CLIENT_ * pClient = g_dbMain.getClientFromList(m_fd);
	if (pClient == NULL) {
		OutputDebug("on download file for fd : %d, but client is not found.", m_fd);
		return;
	}
	pClient->nSocketType = 2;
	pClient->dwRecvTime = g_dbMain.GetTime();

	int ver = 0;
	memcpy(&ver, &data[4], 4);
	OutputDebug("�յ��汾 %d", ver);

	BYTE buff[100] = { 0 };
	int nType = 100;
	memcpy(&buff[0], &nType, 4);
	memcpy(&buff[4], &g_nVer, 4);
	send((char*)buff, 8);
	pClient->dwSendTime = g_dbMain.GetTime();

	OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 8, Byte2String(buff, 8));

	if (ver != g_nVer)
	{
		return;
	}
	DWORD dwStartTime = GetTickCount();
	char name[MAX_PATH] = { 0 };
	char name_t[MAX_PATH] = { 0 };
	sprintf(name, "%s%s", g_dbMain.GetCurrentPathName(), "MonitorData.mdb");
	sprintf(name_t, "%s.tem", name);
	CopyFile(name, name_t, FALSE);
	g_dbMain.ZipFile(name_t);
	char zipFile[MAX_PATH] = { 0 };
	sprintf(zipFile, "%s.zip", name_t);

	DWORD dwEndTime = GetTickCount();
	OutputDebug("Zip File = %d", (dwEndTime - dwStartTime) / 1000);
	CStdioFile file;
	if (file.Open(zipFile, CFile::modeRead | CFile::typeBinary))
	{
		DWORD dwLen = file.GetLength();

		BYTE buff[10] = { 0 };
		memcpy(&buff[0], &dwLen, 4);
		send((char*)buff, 4);
		OutputDebug("�����ļ����� %d", dwLen);
		pClient->dwSendTime = g_dbMain.GetTime();
		OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 4, Byte2String(buff, 4));

		char* pBuff = new char[dwLen + 1];
		memset(pBuff, 0, dwLen + 1);
		file.Read(pBuff, dwLen);
		send(pBuff, dwLen);
		pClient->dwSendTime = g_dbMain.GetTime();
		OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, dwLen, Byte2String((BYTE*)pBuff, dwLen));

		OutputDebug("�����ļ� %d", dwLen);
		file.Close();
		delete[]pBuff;
	}
	DeleteFile(name_t);
	DeleteFile(zipFile);
#endif	
}

void MyConnectionB::onConfigUpdate(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	_CLIENT_ * pClient = g_dbMain.getClientFromList(m_fd);
	if (pClient == NULL) {
		OutputDebug("on config update for fd : %d, but client is not found.", m_fd);
		return;
	}

	int len = 0;
	memcpy(&len, &data[4], 4);
	if (len == (numBytes - 8))
	{
		char *pBuff = new char[len + 1];
		memset(pBuff, 0, len + 1);
		memcpy(pBuff, &data[8], len);

		pClient->dwRecvTime = g_dbMain.GetTime();

		int res  = g_dbMain.ExcuteSQL(pBuff, pClient);
		//OutputDebug("Excute DB %s",pBuff);
		delete[]pBuff;

		if (res == 0)
		{
			int nType = 101;

			int type = 0;
			char buff[1024] = { 0 };
			memcpy(&buff[0], &nType, 4);
			memcpy(&buff[4], &type, 4);
			send(buff, 8);
			OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 8, Byte2String((BYTE*)buff, 8));

			CString strEx(pBuff);
			strEx.MakeReverse();
			if (strEx.Find("WATERWELLDEVICE") >= 0)//���¼��������ݿ�
			{
				if (strEx.Find("INSERT") >= 0 || strEx.Find("UPDATE") >= 0)
				{
					g_dbMain.LoadBaseTables();
				}
			}

		} else if (res == 1) {
			// database is not connected success
			char str[] = { "���ݿ�δ����" };
			int nType = 101;

			int type = 1;
			char buff[1024] = { 0 };
			memcpy(&buff[0], &nType, 4);
			memcpy(&buff[4], &type, 4);
			memcpy(&buff[8], str, strlen(str));
			send(buff, 8 + strlen(str));
			pClient->dwSendTime = g_dbMain.GetTime();
			OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 8 + strlen(str), Byte2String((BYTE*)buff, 8 + strlen(str)));
		} else if (res == 2) {
			// excute database exception
			char str[1024] = { 0 };
			strcpy(str, "excute mysql exception");

			int nType = 101;

			int type = 1;
			char buff[1024] = { 0 };
			memcpy(&buff[0], &nType, 4);
			memcpy(&buff[4], &type, 4);

			int nl = strlen(str);
			if (nl > 1000)
				nl = 1000;
			memcpy(&buff[8], &nl, 4);
			memcpy(&buff[12], str, nl);

			send(buff, 12 + nl);

			OutputDebug("��ͻ��˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 8 + strlen(str), Byte2String((BYTE*)buff, 8 + strlen(str)));
		}
	}
#endif	
}

void MyConnectionB::onDeviceDataToDtu(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	int len = 0;
	memcpy(&len, &data[4], 4);
	if (len == (numBytes - 13))
	{
		char *pBuff = new char[len + 1];

		char DTU_ID[128] = { 0 };
		_DTU_* pDTU;
		memset(DTU_ID, 0x30, 33);
		DTU_ID[0] = '*';	DTU_ID[1] = '*';
		DTU_ID[2] = '#';DTU_ID[3] = '#';DTU_ID[4] = '#';

		unsigned char temp[4];
		unsigned long temp4, temp5;
		temp4 = 0;temp5 = 0;

#ifdef moreThan9999
		memcpy(&temp4, &data[8], 3);
#else
		memcpy(&temp4, &data[8], 2);
#endif
		char temp2[8];
		int jjj = 0;
		//	OutputDebug("�յ��ֿ�BB0 ,%d", temp4);
		temp5 = long_to_4bcd(temp4);
		//	OutputDebug("�յ��ֿ�BB1 ,%d", temp5);
		//	memcpy(&temp[0],&temp5,2); 
		temp[0] = (temp5) & 0xff; temp[1] = (temp5 >> 8) & 0xff;
		temp[2] = (temp5 >> 16) & 0xff; temp[3] = (temp5 >> 24) & 0xff;;
		//	OutputDebug("�յ��ֿ�NNNNNNN ,%d,%d,%d,%d,%d,%d", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);

		//jjj = myhex_2_ascii(temp, temp2, 4);
		//�����ǰ�BCD�ֲ����?X30������Ӧ�õĸ�ʽ
		temp2[5] = (temp[3] & 0x0F) + 0x30;
		temp2[4] = ((temp[3] >> 4) & 0x0F) + 0x30;
		temp2[3] = (temp[2] & 0x0F) + 0x30;
		temp2[2] = ((temp[2] >> 4) & 0x0F) + 0x30;
		temp2[1] = (temp[1] & 0x0F) + 0x30;
		temp2[0] = ((temp[1] >> 4) & 0x0F) + 0x30;

		DTU_ID[27] = temp2[0];
		DTU_ID[28] = temp2[1];
		DTU_ID[29] = temp2[2];
		DTU_ID[30] = temp2[3];
		DTU_ID[31] = temp2[4];
		DTU_ID[32] = temp2[5];


		//**###0000000000000000000000000003 
		//strcpy(DTU_ID,buff);
#ifdef addEncryptionCode
		pDTU = g_dbMain.FindDTU2(DTU_ID);
#else
		pDTU = g_dbMain.FindDTU(DTU_ID);
#endif                       
		if (pDTU == 0) {
			return;//���û�в鵽DTU���˳�
		}


		memset(pBuff, 0, len + 1);
		memcpy(pBuff, &data[13], len);

		pDTU->dwRecvActiveTime = g_dbMain.GetTime();
		if (pDTU->sSocket <= 0) {
			return;
		}
		g_dbMain.sendDataToServerAConnection(pDTU->sSocket, (char*)pBuff, len);
	}
#endif	
}

void MyConnectionB::onHeartBeat(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT	
	_CLIENT_ * pClient = g_dbMain.getClientFromList(m_fd);
	if (pClient == NULL) {
		OutputDebug("on config update for fd : %d, but client is not found.", m_fd);
		return;
	}

	pClient->dwRecvTime = g_dbMain.GetTime(); //�յ���λ������ʱ�����ݵ���������
	if (pClient->nSocketType != 1) {
		return;
	}

	_METER_* pMeter = g_dbMain.getMeterFromList(data[1]);
	if (pMeter == NULL) {
		return;
	}

	pMeter->recv_data[0] = 0x68;
	send((char*)pMeter->recv_data, 32);
	pClient->dwSendTime = g_dbMain.GetTime();
	OutputDebug("��ֿض˷���?%s:%s ���� %d (%s)", pClient->ip, pClient->port, 32, Byte2String(pMeter->recv_data, 32));
#endif	
}
