#include "StdAfx.h"

#include <thread>
#define SERVICE_IMPLEMENT
#ifdef SERVICE_IMPLEMENT
#include "../DataBase.h"
#include "../Util/log.h"
extern CDataBase g_dbMain;
#endif

#include "MyConnectionA.h"

void MyConnectionA::onRead(const char* data, const size_t& numBytes) {
#ifdef SERVICE_IMPLEMENT
	std::thread::id this_id = std::this_thread::get_id();
	OutputDebug("current thread : %d", this_id);
	if (data == NULL || numBytes <= 0) {
		return;
	}
	/*
	std::string sdata(data, numBytes);
	OutputDebug("receive data : %s", sdata.c_str());
	const char* msg = "hoi\n";
	send(msg, strlen(msg));
	*/
	if (data[0] == 0x23) {
		return onHeartBeat(data, numBytes);
	}
	if (numBytes == 32) {
		return onDataToDevices(data, numBytes);
	}
	if (numBytes == 33) {
		return onRegister(data, numBytes);
	}
#endif	
}


void MyConnectionA::onHeartBeat(const char* data, const size_t& numBytes) {
	OutputDebug("on heart beat, data len : %d", numBytes);
#ifdef SERVICE_IMPLEMENT	
	_DTU_* pDTU = g_dbMain.getDtuFromList(m_fd);
	if (pDTU == NULL) {
		OutputDebug("connection A, get dtu from list failed, fd : %d", m_fd);
		return;
	}
	pDTU->dwRecvActiveTime = g_dbMain.GetTime();
	send(data, 1);
	//g_dbMain.WriteLog(0,"�����ط��� %s:%s ���� %d (%s)",pDTU->ip,pDTU->port,1,Byte2String(buff,1));

	char buff[100] = { 0 };
	int type = 0x67;  //���ֽڷ����˸ı䣬��0X68��Ϊ0X67������λ�����մ���

	int len = 12;
	int status = 1;
	long t = CTime::GetCurrentTime().GetTime();
	CString s(pDTU->str_ID);
	int id = atoi(s.Right(4));

	memcpy(&buff[0], &type, 4);
	memcpy(&buff[4], &len, 4);
	memcpy(&buff[8], &id, 4);
	memcpy(&buff[12], &status, 4);
	memcpy(&buff[16], &t, 4);
	g_dbMain.sendDataToServerBConnection(buff, 20);
#endif
}

void MyConnectionA::onRegister(const char* data, const size_t& numBytes) {
	OutputDebug("onRegister, data len : %d", numBytes);
#ifdef SERVICE_IMPLEMENT	
	if (data == NULL || numBytes <= 0) {
		OutputDebug("on register data is null.");
		return;
	}
	std::string dataStr(data, numBytes);
	char* buff = (char*)dataStr.c_str();
	_DTU_* pDTU = g_dbMain.FindDTU((char*)buff);
	if (pDTU)
	{
		char ip[32];
		char port[20];
		struct sockaddr_in ip_adr_get;
		int ip_adr_len;

		ip_adr_len = sizeof(ip_adr_get);
		getpeername(m_fd, (sockaddr*)&ip_adr_get, &ip_adr_len);
		strcpy(ip, inet_ntoa(ip_adr_get.sin_addr));
		sprintf(port, "%d", ntohs(ip_adr_get.sin_port));

		OutputDebug("��֤���� %s:%s %s", ip, port, buff);
		pDTU->sSocket = m_fd;
		strcpy(pDTU->ip, ip);
		strcpy(pDTU->port, port);
		pDTU->dwConnectTime = g_dbMain.GetTime();
		memset(pDTU->m_strLastRecv, 0, sizeof(pDTU->m_strLastRecv));
		pDTU->m_nLastLen = 0;
	}
	else
	{
		OutputDebug("��֤���� ����");
	}
#endif	
}

void MyConnectionA::onDataToDevices(const char* data, const size_t& numBytes) {
	OutputDebug("onDataToDevices, data len : %d", numBytes);
#ifdef SERVICE_IMPLEMENT	
	_DTU_* pDTU = g_dbMain.getDtuFromList(m_fd);

	char buffnn[32];;
	if (pDTU == NULL) {
		OutputDebug("connection A, get dtu from list failed, fd : %d", m_fd);
		return;
	}
	memcpy(buffnn, data, 32);
	char* buff = (char*)data;
	size_t nLen = numBytes;
	WORD dddd = g_dbMain.Caluation_CRC16((BYTE*)buffnn, nLen - 3);
	BYTE bb[2] = { 0 };
	BYTE bb2[2] = { 0 };
	memcpy(bb, &dddd, 2);

		bb2[0] = buff[29];
		bb2[1] = buff[30];

		
	if (bb[0] == bb2[0] && bb[1] == bb2[1])
	{
		//check ok
		_METER_* pMeter = g_dbMain.FindMeter(pDTU, buff[1]);
		if (!pMeter)
		{
			OutputDebug("connection A, get meter from list failed, fd : %d", m_fd);
			return;
		}
		memcpy(pMeter->recv_data, buff, 32);
		pMeter->dwRecvMeterDataTime = g_dbMain.GetTime();

		long l = g_dbMain.GetTime();
		char strSend[1024] = { 0 };
		int nType = 0x69;
		memcpy(&strSend[0], &nType, 4);
		int len = nLen + 12;
		memcpy(&strSend[4], &len, 4);
		memcpy(&strSend[8], &pMeter->nDTU_ID, 4);
		memcpy(&strSend[12], &pMeter->m_nType, 4);
		memcpy(&strSend[16], &l, 4);
		memcpy(&strSend[20], buff, nLen);

		g_dbMain.sendDataToServerBConnectionBySocketType(1, strSend, len + 8);
		
		if (pMeter->m_nType == 1)
		{
			_DATA_METER_1* pData = &pMeter->data1;

			int d1 = 0, d2 = 0;

			pData->nID = buff[1];

			d1 = buff[5], d2 = buff[6], pData->nUedE = d1 * 256 + d2;
			pData->nUedE_E = buff[7];

			d1 = buff[8], d2 = buff[9], pData->nLeftE = d1 * 256 + d2;
			pData->nLeftE_E = buff[10];

			d1 = buff[11], d2 = buff[12], pData->nUsedM = d1 * 256 + d2;
			pData->nUsedM_E = buff[13];

			d1 = buff[14], d2 = buff[15], pData->nLeftM = d1 * 256 + d2;
			pData->nLeftM_E = buff[16];

			d1 = buff[17], d2 = buff[18], pData->nRate = d1 * 256 + d2;
			pData->nRate_E = buff[19];

			d1 = buff[20], d2 = buff[21], pData->nFlow = d1 * 256 + d2;
			pData->nFlow_E = buff[22];

			d1 = buff[23], d2 = buff[24], pData->nFlow2 = d1 * 256 + d2;
			pData->nFlow2_E = buff[25];

			d1 = buff[26], d2 = buff[27], pData->nUID = d1 * 256 + d2;
			pData->nUID_E = buff[28];

			pData->lTime = g_dbMain.GetTime();
		}
		else if (pMeter->m_nType == 2)
		{
			_DATA_METER_2* pData = &pMeter->data2;

			int d1 = 0, d2 = 0, d3 = 0;

			pData->nID2 = buff[1];

			d1 = buff[5], d2 = buff[6], d3 = buff[7], pData->nUedE2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[8], d2 = buff[9], d3 = buff[10], pData->nLeftE2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[11], d2 = buff[12], d3 = buff[13], pData->nUsedM2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[14], d2 = buff[15], d3 = buff[16], pData->nLeftM2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[17], d2 = buff[18], d3 = buff[19], pData->nRate2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[20], d2 = buff[21], d3 = buff[22], pData->nFlowTotal2 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[23], d2 = buff[24], d3 = buff[25], pData->nFlow22 = (d1 * 256 + d2) * 256 + d3;

			d1 = buff[26], d2 = buff[27], d3 = buff[28], pData->nUID2 = (d1 * 256 + d2) * 256 + d3;

			pData->lTime = g_dbMain.GetTime();
		}
		else if (pMeter->m_nType == 3)
		{
			_DATA_RTU* pData = &pMeter->data3;

			int d1 = 0, d2 = 0;

			pData->nID3 = buff[1];

			d1 = buff[5], d2 = buff[6], pData->nFlow = d1 * 256 + d2;
			pData->nFlow_E = buff[7];

			d1 = buff[8], d2 = buff[9], pData->nWater = d1 * 256 + d2;
			pData->nWater_E = buff[10];

			d1 = buff[11], d2 = buff[12], pData->nTempr = d1 * 256 + d2;
			pData->nTempr_E = buff[13];

			d1 = buff[14], d2 = buff[15], pData->nHumidity = d1 * 256 + d2;
			pData->nHumidity_E = buff[16];

			d1 = buff[17], d2 = buff[18], pData->nOther1 = d1 * 256 + d2;
			pData->nOther1_E = buff[19];

			d1 = buff[20], d2 = buff[21], pData->nOther2 = d1 * 256 + d2;
			pData->nOther2_E = buff[22];

			d1 = buff[23], d2 = buff[24], pData->nOther3 = d1 * 256 + d2;
			pData->nOther3_E = buff[25];

			d1 = buff[26], d2 = buff[27], pData->nOther4 = d1 * 256 + d2;
			pData->nOther4_E = buff[28];

			pData->lTime = g_dbMain.GetTime();
		}
	}
#endif	
}